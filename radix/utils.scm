(define-module (radix utils)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:use-module (ice-9 match)
  #:use-module (ice-9 ftw)
  #:export (associate-left
            associate-right
            flat-map
            list-contents
            almost-basename))

(define-syntax-rule
   (associate-left (key associations) ...)
  `(,@(map (cut cons key <>) associations) ...))

(define-syntax-rule
   (associate-right (association keys) ...)
  `(,@(map (cut cons <> association) keys) ...))

(define (flat-map proc . lsts)
  "Returns a list containg every other application of PROC to a list where the
first element comes from the first list, the second element from the second
list, up to the last list."
  (match lsts
    ((head-lst) (map proc head-lst))
    ((head-lst . tail-lsts)
     (append-map (lambda (h)
                   (apply flat-map
                          (lambda arglst (apply proc h arglst))
                          tail-lsts))
                 head-lst))))

(define (list-contents dir)
  (and=> (scandir dir)
         (lambda (subdirs)
           (map (cut string-append dir "/" <>)
                (cddr subdirs)))))

(define (almost-basename file)
  (string-append (basename (dirname file))
                 "/" (basename file)))
