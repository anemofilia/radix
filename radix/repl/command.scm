(define-module (radix repl command)
  #:use-module (guix packages)
  #:use-module (system repl command)
  #:use-module (srfi srfi-1))

(define-meta-command ((clear radix) repl)
  "clear
Clear repl."
  (system* "printf" "\\033c"))

(define-meta-command ((edit radix) repl filename)
  "edit FILENAME
Open FILENAME using the editor specified by (getenv \"EDITOR\")."
  (system* (getenv "EDITOR") filename))

(define-meta-command ((home radix) repl)
  "home
Open $XDG_CONFIG_HOME/guix/home.scm using the editor specified by (getenv \"EDITOR\")."
  (system* (getenv "EDITOR")
           (string-append (getenv "XDG_CONFIG_HOME")
                          "/guix/home.scm")))

(define-meta-command ((system radix) repl)
  "system
Open /etc/config.scm using the editor specified by (getenv \"EDITOR\")."
  (system* (getenv "EDITOR") "/etc/config.scm"))

(define (module-path module)
  (false-if-exception
    (find file-exists?
          (map (lambda (load-dir)
                 (string-append load-dir "/" (module-filename module)))
               %load-path))))

(define (interface-path interface)
  (module-path
    (resolve-module
      (module-name interface))))

(define-meta-command ((visit module) repl module)
  "visit MODULE
Open MODULE source file using the editor specified by (getenv \"EDITOR\").
Returns #f if no file defining MODULE is found in %load-path."
  (and=> (module-path (resolve-module module))
         (lambda (path) (system* (getenv "EDITOR") path))))

(define-meta-command ((definition radix) repl symbol)
  "definition SYMBOL
Open the module that defines SYMBOL using the editor specified by (getenv \"EDITOR\")."
  (and=> (find (lambda (interface)
                   (not (false-if-exception
                          (module-ref interface symbol))))
               (module-uses (current-module)))
         (lambda (interface)
           (system* (getenv "EDITOR")
                    (interface-path interface)))))
