(define-module (radix system setuid)
  #:use-module (gnu)
  #:use-module (ice-9 match)
  #:use-module (radix utils)
  #:use-module (gnu system setuid)
  #:export (map-setuid-programs))

(define-syntax-rule
  (map-setuid-programs (package package-programs) clause ...)
  (map (match-lambda ((package-program . package)
                      (setuid-program
                       (program 
                        (file-append package
                                     (string-append "/bin/" package-program))))))
       (associate-right (package package-programs) clause ...)))
