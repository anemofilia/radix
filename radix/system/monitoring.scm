(define-module (radix system monitoring)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:use-module (ice-9 ftw)
  #:use-module (ice-9 match)
  #:use-module (ice-9 popen)
  #:use-module (ice-9 rdelim)
  #:use-module (ice-9 regex)
  #:use-module (ice-9 textual-ports)
  #:use-module (ice-9 threads)

  #:export (current-memory-info
            current-ram-usage
            current-swap-usage
            ram-total

            current-temperature

            current-cpu-usage
            current-cpu-frequency

            current-battery-status
            current-battery-capacity

            sink-volume

            current-net-download
            current-net-upload
            current-net-download+upload))

#|Ports|#
(define get-lines-all
  (compose (cut string-split <> #\newline)
           (cut string-drop-right <> 1)
           get-string-all))

(define (get-line-n port n)
  (map (lambda _ (get-line port)) (iota n)))

(define (read-file file)
  (call-with-input-file file read))

#|Memory|#
(define (current-memory-info)
  (map (lambda (line)
         (apply cons
                (map (cut with-input-from-string <> read)
                     (string-split line #\:))))
       (call-with-input-file "/proc/meminfo" get-lines-all)))

(define (ram-total)
  "Returns the total available ram in bytes."
  (call-with-input-file "/proc/meminfo"
    (lambda (port)
      (let ((_ (read port)))
        (* 1000 (read port))))))

(define (current-ram-usage)
  (apply - (map (cut assoc-ref (current-memory-info) <>)
                '(MemTotal MemFree Buffers Cached SReclaimable))))

(define (current-swap-usage)
  (apply - (map (cut assoc-ref (current-memory-info) <>)
                '(SwapTotal SwapFree))))

#|battery usage monitoring utilities|#
(define (current-battery-status)
  (let* ((power-supply-dir "/sys/class/power_supply")
         (batteries (filter (cut string-prefix? "BAT" <>)
                            (scandir power-supply-dir))))
    (apply values
           (map (lambda (battery)
                  (let ((status-file
                         (string-append power-supply-dir "/" battery "/status")))
                    (call-with-input-file status-file get-line)))
                batteries))))

(define (current-battery-capacity)
  (let* ((power-supply-dir "/sys/class/power_supply")
         (batteries (filter (cut string-prefix? "BAT" <>)
                            (scandir power-supply-dir)))
         (battery-capacities
          (map (lambda (battery)
                 (let ((capacity-file
                        (string-append power-supply-dir "/" battery "/capacity")))
                   (call-with-input-file capacity-file read)))
               batteries)))
     (apply values battery-capacities)))

#|temperature monitoring utilities|#
(define (current-temperature)
  "Returns the temperatures, in celsius degree, of every"
  (let* ((thermal-dir
          "/sys/class/thermal")
         (thermal-zones
           (filter (cut string-prefix? "thermal_zone" <>)
                   (scandir thermal-dir)))
         (temperatures
          (map (lambda (thermal-zone)
                 (let ((temp-file
                        (string-append thermal-dir "/" thermal-zone "/temp")))
                   (round/ (call-with-input-file temp-file read) 1000)))
               thermal-zones)))
    (apply values temperatures)))

#|cpu monitoring utilities|#
(define (numbers str)
  (map (compose string->number match:substring)
       (list-matches "[0-9]+(\\.[0-9]*|)" str)))

(define (number-of-cores)
  (call-with-input-file "/sys/devices/system/cpu/present"
    (compose 1+ second numbers get-line)))

(define (current-cpu-usage)
  (define (stat)
    (let ((stat-lines
            (call-with-input-file "/proc/stat"
              (lambda (port) (get-line-n port (1+ (number-of-cores)))))))
      (map (lambda (stat-line) (take (numbers stat-line) 7))
           stat-lines)))
  (let* ((a (stat))
         (_ (usleep #e1e6))
         (b (stat))
         (c (map (cut map - <> <>) b a)))
    (apply values
           (map (lambda (c-line)
                  (match c-line
                    ((c0 c1 c2 c3 c4 c5 c6)
                     (round (* 100 (/ (+ c0 c1 c2 c5 c6)
                                      (apply + c-line)))))))
                 c))))

(define (current-cpu-frequency)
  (apply values
         (map (lambda (x)
                (call-with-input-file
                  (format #f "/sys/devices/system/cpu/cpu~d/cpufreq/scaling_cur_freq" x)
                  read))
             (iota (number-of-cores)))))

#|Volume|#
(define* (sink-volume #:optional (sink "@DEFAULT_AUDIO_SINK@"))
  (let* ((cmd (string-append "wpctl get-volume " sink))
         (output (call-with-port (open-input-pipe cmd) get-line))
         (volume-percentage (first (numbers output))))
    (values (inexact->exact (* 100 volume-percentage))
            (if (string-suffix? "[MUTED]" output) "Muted" "Unmuted"))))

#|network|#
(define (current-net-download interface)
  (let* ((rx-bytes-file
          (format #f "/sys/class/net/~a/statistics/rx_bytes" interface))
         (rx-bytes-old (read-file rx-bytes-file))
         (_ (sleep 1))
         (rx-bytes-new (read-file rx-bytes-file)))
    (- rx-bytes-new rx-bytes-old)))

(define (current-net-upload interface)
  (let* ((tx-bytes-file
          (format #f "/sys/class/net/~a/statistics/tx_bytes" interface))
         (tx-bytes-old (read-file tx-bytes-file))
         (_ (sleep 1))
         (tx-bytes-new (read-file tx-bytes-file)))
    (- tx-bytes-new tx-bytes-old)))

(define (current-net-download+upload interface)
  (parallel (current-net-download "wlp2s0")
            (current-net-upload "wlp2s0")))
