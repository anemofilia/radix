(define-module (radix services repl)
  #:use-module (guix gexp)
  #:use-module (gnu services)
  #:use-module (gnu services shepherd)

  #:export (shepherd-repl-service-type))

(define (shepherd-repl-service _)
  (shepherd-service
    (provision '(repl))
    (modules '((shepherd service repl)))
    (free-form #~(repl-service))))

(define shepherd-repl-service-type
  (service-type
   (name 'shepherd-repl)
   (extensions
    (list (service-extension shepherd-root-service-type
                             (compose list shepherd-repl-service))))
   (default-value #t)
   (description "Run a read-eval-print loop (REPL).")))
