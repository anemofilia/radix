(define-module (radix services shepherd)
  #:use-module (guix gexp)
  #:use-module (guix records)
  #:use-module (gnu services)
  #:use-module (gnu services configuration)
  #:use-module (gnu services shepherd)

  #:export (radix:shepherd-timer-service-type
            shepherd-timer
            shepherd-timer-name
            shepherd-timer-event
            shepherd-timer-action
            shepherd-timer-log-file
            shepherd-timer-modules
            shepherd-timer-max-duration
            shepherd-timer-occurrences
            shepherd-timer-wait-for-termination?))

(define shepherd-timer-trigger-action
  (shepherd-action
    (name 'trigger)
    (procedure #~trigger-timer)
    (documentation "Trigger the action associated with @var{timer} as if it had
reached its next calendar event.")))

(define (log-file? x)
  (or (string? x) (not x)))

(define (duration? x)
  (or (number? x) (not x)))

(define-record-type* <shepherd-timer>
  shepherd-timer make-shepherd-timer shepherd-timer?
  (name shepherd-timer-name)                                  ; symbol
  (event shepherd-timer-event)                                ; gexp
  (action shepherd-timer-action)                              ; gexp
  (occurrences shepherd-timer-occurrences                     ; number
               (default +inf.0))
  (modules shepherd-timer-modules                             ; list of module names
           (default '()))
  (log-file shepherd-timer-log-file                           ; string | #f
            (default #f))
  (max-duration shepherd-timer-max-duration                   ; number | #f
                (default #f))
  (provide-trigger? shepherd-timer-provide-trigger?           ; boolean
                    (default #t))
  (wait-for-termination? shepherd-timer-wait-for-termination? ; boolean
                         (default #f)))

(define (shepherd-timer-service config)
  (map (match-record-lambda <shepherd-timer>
        (name event action occurrences log-file modules max-duration
         wait-for-termination? provide-trigger?)
        (shepherd-service
          (provision (list name))
          (modules `((shepherd service timer)
                     ,@modules))
          (start #~(make-timer-constructor
                     #$event
                     #$action
                     #:occurrences #$occurrences
                     #:log-file #$log-file
                     #:max-duration #$max-duration
                     #:wait-for-termination? #$wait-for-termination?))
          (stop #~(make-timer-destructor))
          (actions (if provide-trigger?
                     (list shepherd-timer-trigger-action)
                     '()))))
       config))

(define radix:shepherd-timer-service-type
  (service-type
   (name 'radix:shepherd-timer)
   (extensions (list (service-extension shepherd-root-service-type
                                        shepherd-timer-service)))
   (default-value '())
   (description "Spawn programs at specified times.")))
