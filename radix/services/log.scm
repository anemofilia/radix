(define-module (radix services log)
  #:use-module (guix gexp)
  #:use-module (guix records)
  #:use-module (gnu services)
  #:use-module (gnu services configuration)
  #:use-module (gnu services shepherd)

  #:export (system-log-service-type
            log-rotation-service-type))

(define-maybe/no-serialization parameter)
(define-maybe/no-serialization procedure)
(define-maybe/no-serialization string)

(define-configuration/no-serialization shepherd-system-log-configuration
  (provision
   (list-of-symbols '(system-log syslogd))
   "A list of symbols denoting what the service provides.")

  (requirement
   (list-of-symbols '())
   "a list of symbols denoting what the service requires.")

  (kernel-log-file
   maybe-parameter
   "A SRFI-39 parameter denotes the location of the kernel log file")

  (message-destination
   maybe-procedure
   "A procedure that, given a system log message, returns a \"good\" default
destination to log it to.")

  (date-format
   maybe-string
   "A 'strftime' format string to prefix each entry in the log.")

  (history-size
   maybe-parameter
   "Number of lines of service log kept in memory.")

  (max-silent-time
   maybe-parameter
   "Maximum number of seconds during which syslogd should remain silent."))

(define (shepherd-system-log-service config)
  (match-record config <shepherd-system-log-configuration>
    (provision requirement kernel-log-file message-destination
     date-format history-size max-silent-time)
    (shepherd-service
     (documentation "Shepherd's built-in system log (syslogd).")
     (provision provision)
     (modules '((shepherd service system-log)))
     (free-form #~(system-log-service
                    #:provision #$provision
                    #:requirement #$requirement
                    #:kernel-log-file
                    (if #$(maybe-value-set? kernel-log-file)
                      #$kernel-log-file
                      (kernel-log-file))
                    #:message-destination
                    (if #$(maybe-value-set? message-destination)
                      #$message-destination
                      (default-message-destination-procedure))
                    #:date-format
                    (if (maybe-value-set? #$date-format)
                      #$date-format
                      default-logfile-date-format)
                    #:history-size
                    (if (maybe-value-set? #$history-size)
                      #$history-size
                      (default-log-history-size))
                    #:max-silent-time
                    (if (maybe-value-set? #$max-silent-time)
                      #$max-silent-time
                      (default-max-silent-time)))))))

(define system-log-service-type
  (service-type
   (name 'shepherd-system-log)
   (extensions
    (list (service-extension shepherd-root-service-type
                             (compose list shepherd-system-log-service))))
   (default-value (shepherd-system-log-configuration))
   (description "Shepherd's built-in system log (syslogd).")))

#|Log rotation|#
(define-configuration/no-serialization shepherd-log-rotation-configuration
  (event
   gexp
   "A gexp of calendar event as returned by ‘calendar-event’")

  (provision
   (list-of-symbols '(log-rotation))
   "A list of symbols denoting what the service provides.")

  (requirement
   (list-of-symbols '())
   "A list of symbols denoting what the service requires.")

  (external-log-files
   (list-of-strings '())
   "A list of file names such as ‘/var/log/nginx/access.log’ corresponding to
\"external\" log files not passed as ‘#:log-file’ to any service) on every
occurrence of EVENT, a calendar event.")

  (compression
   maybe-parameter
   "The compress method to be used for log files. Can be one of ‘'gzip’,
‘'zstd’, ‘'none’, or a one-argument procedure that is passed the file name.")

  (expiry
   maybe-parameter
   "Default duration in seconds after which log files are deleted.")

  (rotation-size-threshold
   maybe-parameter
   "Default size in bytes below which log files are not considered for
rotation."))

(define (shepherd-log-rotation-service config)
  (match-record config <shepherd-log-rotation-configuration>
    (event provision requirement external-log-files compression expiry
     rotation-size-threshold)
    (shepherd-service
     (provision '(log-rotation))
     (modules '((shepherd service log-rotation)))
     (free-form #~(log-rotation-service event
                    #:provision #$provision
                    #:requirement #$requirement
                    #:external-log-files #$external-log-files
                    #:compression
                    (if #$(maybe-value-set? compression)
                      #$compression
                      (default-log-compression))
                    #:expiry
                    (if #$(maybe-value-set? expiry)
                      #$expiry
                      (default-log-expiry))
                    #:rotation-size-threshold
                    (if #$(maybe-value-set? expiry)
                      #$expiry
                      (default-rotation-size-threshold)))))))

(define log-rotation-service-type
  (service-type
   (name 'shepherd-log-rotation)
   (extensions
    (list (service-extension shepherd-root-service-type
                             (compose list shepherd-log-rotation-service))))
   (default-value #t)
   (description "Periodically rotate the log files of services.")))
