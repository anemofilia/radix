(define-module (radix services admin)
  #:use-module (gnu packages admin)
  #:use-module (gnu services configuration)
  #:use-module ((gnu services) #:hide (delete))
  #:use-module (gnu system privilege)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix records)
  #:use-module (ice-9 format)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:export (opendoas-service-type
            opendoas-configuration
            opendoas-rule-extension

            permit
            deny))

;; Dummy serializers, just to avoid warnings
(define empty-serializer
  (@@ (gnu services configuration) empty-serializer))
(define serialize-string empty-serializer)
(define serialize-list-of-strings empty-serializer)

(define assoc-list? (list-of pair?))
(define (serialize-assoc-list field-name val)
  (map (match-lambda
         ((var . #t) var)
         ((var . #f) (string-append "-" var))
         ((var . value) (format #f "~a=~a" var value)))
       val))
(define-maybe list-of-strings)
(define-maybe assoc-list)
(define-maybe string)

(define-configuration/no-serialization permit-rule
  (nopass?
   (boolean #f)
   "Whether the user should be permitted to run the command without a password.")
  (nolog?
   (boolean #f)
   "Wheter sucessful command exection should be logged.")
  (persist?
   (boolean #f)
   "After the user sucessfully authenticates, do not ask for a password again
for some time.")
  (keepenv?
   (boolean #f)
   "Wheter environment variables other than those listed in doas should be
retained when creating the enviroment for the new process.")
  (identity
   string
   "The username to match. Groups may be specified by prepending a colon ':'.")
  (as-user
   maybe-string
   "The target user the running user is allowed to run the command as.  The
default is all users.")
  (command
   maybe-string
   "The command the user is allowed to run.  The default is all commands.
It's preferable to have commands specifieds by absolute paths. If a relative
path is specified, only a restricted PATH will be searched.")
  (args
   maybe-list-of-strings
   "Arguments to command.  The command arguments provided by the user need to
match those specified.  The keyword args alone means that command must be run
without arguments.")
  (setenv
   maybe-assoc-list
   "Set the specified variables.  Variables may also be removed by setting them
to #f, or simply exported, by setting them to #t.  If the first character of the
value is ‘$’ then the value to be set is taken from the existing environment
variable with the given name."))
(define-syntax-rule (permit field ...)
  (permit-rule field ...))

(define (unset? val)
  "Tests if VAL is unset."
  (equal? val (@@ (gnu services configuration)
                  %unset-value)))

(define* (if-set val #:optional (proc identity))
  "Apply PROC to VAL if VAL is not unset, otherwise returns #f."
  (if (not (unset? val)) (proc val) #f))

(define serialize-permit-rule
  (match-record-lambda <permit-rule>
    (identity as-user command args setenv keepenv? nopass? nolog? persist?)
    (format #f "permit ~:[~;keepenv ~]~
                       ~:[~;nopass ~]~
                       ~:[~;nolog ~]~
                       ~:[~;persist ~]~
                       ~@[setenv {~{ ~a~} } ~]~
                       ~a~@[ as ~a~]~
                         ~@[ cmd ~a~]~
                         ~@[ args~{ ~a~}~]~%"
            keepenv?
            nopass?
            nolog?
            persist?
            (if-set setenv (cut serialize-assoc-list #f <>))
            identity
            (if-set as-user)
            (if-set command)
            (if-set args))))

(define-configuration/no-serialization deny-rule
  (identity
   string
   "The username to match. Groups may be specified by prepending a colon ':'.")
  (as-user
   maybe-string
   "The target user the running user is allowed to run the command as.  The
default is all users.")
  (command
   maybe-string
   "The command the user is allowed to run.  The default is all commands.
It's preferable to have commands specifieds by absolute paths. If a relative
path is specified, only a restricted PATH will be searched.")
  (args
   maybe-string
   "Arguments to command.  The command arguments provided by the user need to
match those specified.  The keyword args alone means that command must be run
without arguments."))
(define-syntax-rule (deny field ...)
  (deny-rule field ...))

(define serialize-deny-rule
  (match-record-lambda <deny-rule>
    (identity as-user command args)
    (format #f "deny ~a~@[ as ~a~]~@[ cmd ~a~]~@[ args~{ ~a~}~]~%"
            identity
            (if-set as-user)
            (if-set command)
            (if-set args))))

(define (rule? x)
  (or (permit-rule? x)
      (deny-rule? x)))

(define list-of-rules?
  (list-of rule?))

(define-configuration/no-serialization opendoas-configuration
  (opendoas
    (package opendoas)
    "The doas package to be used.")
  (rules
    (list-of-rules '())
    "The list of permit and/or deny rules used by doas."))

(define-configuration/no-serialization opendoas-rule-extension
  (rules
    (list-of-rules '())
    "The list of permit and/or deny rules used by doas."))

(define (opendoas-extensions original-config extension-configs)
  (opendoas-configuration
    (inherit original-config)
    (rules (append (opendoas-configuration-rules original-config)
                   (reverse (append-map opendoas-rule-extension-rules
                                        extension-configs))))))

(define (opendoas-config-file config)
  `(("doas.conf"
     ,(plain-file "doas.conf"
        (apply string-append
               (map (lambda (s)
                      (cond ((permit-rule? s)
                             (serialize-permit-rule s))
                            ((deny-rule? s)
                             (serialize-deny-rule s))))
                    (opendoas-configuration-rules config)))))))

(define (opendoas-privileged-programs config)
  (let ((opendoas (opendoas-configuration-opendoas config)))
    (list (privileged-program
            (program (file-append opendoas "/bin/doas"))
            (setuid? #t)))))

(define (opendoas-packages config)
  (list (opendoas-configuration-opendoas config)))

(define opendoas-service-type
  (service-type (name 'opendoas)
                (extensions
                 (list (service-extension etc-service-type
                                          opendoas-config-file)
                       (service-extension profile-service-type
                                          opendoas-packages)
                       (service-extension privileged-program-service-type
                                          opendoas-privileged-programs)))
                (compose identity)
                (extend opendoas-extensions)
                (default-value (opendoas-configuration))
                (description "Set /etc/doas.conf")))
