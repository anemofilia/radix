(define-module (radix artwork)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:export (%artwork-repository))

(define %artwork-repository
  (let ((commit "92e0fc25524999032fbd32c30bede6b10d03d620"))
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://codeberg.org/anemofilia/radix-artwork.git")
           (commit commit)))
     (file-name
      (string-append "radix-artwork-" (string-take commit 7) "-checkout"))
     (sha256 (base32 "083frd744b8qa9fcdma4s0031sxbxdb5xih1vn3gwjwn6jy8hvj3")))))
