(define-module (radix packages)
  #:use-module (gnu packages)
  #:use-module (radix combinators)
  #:export (%patch-path))

(define %patch-path
  (and=> (search-path %load-path "radix/packages.scm")
         (compose list
                  (partial (flip string-append)
                           "/packages/patches")
                  dirname)))
