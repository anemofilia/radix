(define-module (radix packages video)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (gnu packages video))

(define-public mpv-minimal/x11
  (package/inherit mpv
    (name "mpv-minimal-x11")
    (arguments
      (substitute-keyword-arguments (package-arguments mpv)
        ((#:configure-flags flags)
         #~(list "-Dlibmpv=true"
                 "-Dbuild-date=false"))))
    (propagated-inputs
      (modify-inputs (package-propagated-inputs mpv)
        (delete "jack")
        (delete "libbluray")
        (delete "libcdio-paranoia")
        (delete "libdvdnav")
        (delete "wayland")
        (delete "wayland-protocols")
        (delete "libdvdread")
        (delete "pulseaudio")))
    (synopsis "Audio and video player")
    (description "This package provides a minimal build of mpv, the
general-purpose audio and video player, for X11.")))

(define-public mpv-minimal/wayland
  (package/inherit mpv
    (name "mpv-minimal-wayland")
    (arguments
      (substitute-keyword-arguments (package-arguments mpv)
        ((#:configure-flags flags)
         #~(list "-Dlibmpv=true"
                 "-Dbuild-date=false"))))
    (propagated-inputs
      (modify-inputs (package-propagated-inputs mpv)
        (delete "jack")
        (delete "libbluray")
        (delete "libcdio-paranoia")
        (delete "libdvdnav")
        (delete "libdvdread")
        (delete "libext")
        (delete "libx11")
        (delete "libxinerama")
        (delete "libxpresent")
        (delete "libxrandr")
        (delete "libxscrnsaver")
        (delete "libxv")
        (delete "pulseaudio")))
    (synopsis "Audio and video player")
    (description "This package provides a minimal build of mpv, the
general-purpose audio and video player, for wayland.")))
