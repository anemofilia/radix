(define-module (radix packages boost)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix utils)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system trivial)
  #:use-module (gnu packages)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages boost)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages icu4c)
  #:use-module (gnu packages llvm)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages python)
  #:use-module (gnu packages shells)
  #:use-module (gnu packages mpi))

(define version-with-underscores
  (@@ (gnu packages boost)
      version-with-underscores))

(define boost-patch
  (@@ (gnu packages boost)
      boost-patch))

(define-public boost/newer
  (package
    (name "boost")
    (version "1.85.0")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://boostorg.jfrog.io/artifactory/main/release/"
                                  version "/source/boost_"
                                  (version-with-underscores version) ".tar.bz2"))
              (sha256
               (base32
                "05w63ybpn23b13asl2vvkf24xf5d5cx709vhvimlg5qnm8gzw2bh"))))
    (build-system gnu-build-system)
    (inputs
     (append
      (list icu4c zlib)
      (if (%current-target-system)
          '()
          (list python-minimal-wrapper))))
    (native-inputs
     (list perl tcsh))
    (arguments
     (list
      #:imported-modules `((guix build python-build-system)
                           ,@%default-gnu-imported-modules)
      #:modules `(((guix build python-build-system) #:select (python-version))
                  ,@%default-gnu-imported-modules)
      #:tests? #f
      #:configure-flags
      #~(let ((icu (dirname (dirname (search-input-file
                                      %build-inputs "bin/uconv")))))
          (list
           ;; Auto-detection looks for ICU only in traditional
           ;; install locations.
           (string-append "--with-icu=" icu)
           ;; Ditto for Python.
           #$@(if (%current-target-system)
                  #~()
                  #~((let ((python (dirname (dirname (search-input-file
                                                      %build-inputs
                                                      "bin/python")))))
                       (string-append "--with-python-root=" python)
                       (string-append "--with-python=" python
                                      "/bin/python")
                       (string-append "--with-python-version="
                                      (python-version python)))))
           "--with-toolset=gcc"))
      #:make-flags
      #~(list "threading=multi" "link=shared"

              ;; Set the RUNPATH to $libdir so that the libs find each other.
              (string-append "linkflags=-Wl,-rpath="
                             #$output "/lib")
              #$@(if (%current-target-system)
                     #~("--user-config=user-config.jam"
                        ;; Python is not supported when cross-compiling.
                        "--without-python"
                        "binary-format=elf"
                        "target-os=linux"
                        #$@(cond
                            ((string-prefix? "arm" (%current-target-system))
                             #~("abi=aapcs"
                                "address-model=32"
                                "architecture=arm"))
                            ((string-prefix? "aarch64" (%current-target-system))
                             #~("abi=aapcs"
                                "address-model=64"
                                "architecture=arm"))
                            (else #~())))
                     #~()))
      #:phases
      #~(modify-phases %standard-phases
          (add-after 'unpack 'patch-shells
            (lambda _
              (substitute* '("libs/config/configure"
                             "libs/spirit/classic/phoenix/test/runtest.sh"
                             "tools/build/src/engine/execunix.cpp")
                (("/bin/sh") (which "sh")))))
          (delete 'bootstrap)
          (replace 'configure
            (lambda* (#:key (configure-flags ''()) #:allow-other-keys)
              (setenv "SHELL" (which "sh"))
              (setenv "CONFIG_SHELL" (which "sh"))

              #$@(if (%current-target-system)
                     #~((call-with-output-file "user-config.jam"
                          (lambda (port)
                            (format port
                                    "using gcc : cross : ~a-c++ ;"
                                    #$(%current-target-system)))))
                     #~())

              (apply invoke "./bootstrap.sh"
                     (string-append "--prefix=" #$output)
                     configure-flags)))
          (replace 'build
            (lambda* (#:key make-flags #:allow-other-keys)
              (apply invoke "./b2"
                     (format #f "-j~a" (parallel-job-count))
                     make-flags)))
          (replace 'install
            (lambda* (#:key make-flags #:allow-other-keys)
              (apply invoke "./b2" "install" make-flags)))
          #$@(if (%current-target-system)
                 #~()
                 #~((add-after 'install 'provide-libboost_python
                      (lambda* (#:key make-flags inputs outputs #:allow-other-keys)
                        (let* ((static? (member "link=static" make-flags))
                               (libext (if static? ".a" ".so"))
                               (python (dirname (dirname (search-input-file
                                                          inputs "bin/python"))))
                               (python-version (python-version python))
                               (libboost_pythonNN
                                (string-append "libboost_python"
                                               (string-join (string-split
                                                             python-version #\.)
                                                            "")
                                               libext)))
                          (with-directory-excursion (string-append #$output "/lib")
                            (symlink libboost_pythonNN
                                     (string-append "libboost_python" libext))
                            ;; Some packages only look for the major version.
                            (symlink libboost_pythonNN
                                     (string-append "libboost_python"
                                                    (string-take python-version 1)
                                                    libext)))))))))))

    (home-page "https://www.boost.org")
    (synopsis "Peer-reviewed portable C++ source libraries")
    (description
     "A collection of libraries intended to be widely useful, and usable
across a broad spectrum of applications.")
    (license (license:x11-style "https://www.boost.org/LICENSE_1_0.txt"
                                "Some components have other similar licences."))))
