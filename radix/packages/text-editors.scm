(define-module (radix packages text-editors)
  #:use-module (guix build-system trivial)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module ((guix licenses)
                #:prefix license:))

(define-public kakoune
  (let ((base (@@ (gnu packages text-editors) kakoune)))
    (package
     (inherit base)
     (source #f)
     (build-system trivial-build-system)
     (native-inputs (list base))
     (arguments
       (list #:modules '((guix build utils))
             #:builder
             #~(begin
                 (use-modules (guix build utils))
                 (let* ((out #$output)
                        (applications (string-append out "/share/applications"))
                        (tools (string-append out "/share/kak/autoload/tools"))
                        (in (assoc-ref %build-inputs "kakoune"))
                        (kak-bin (string-append out "/bin/kak")))
                   (copy-recursively in out)
                   (chmod kak-bin #o755)
                   (mkdir-p applications)
                   (make-desktop-entry-file
                     (string-append applications "/kak.desktop")
                     #:type "Application"
                     #:name "kak"
                     #:comment "Launches the kakoune text editor"
                     #:icon "utilities-terminal"
                     #:terminal #t
                     #:try-exec (string-append out "/bin/kak")
                     #:exec (string-append out "/bin/kak %f")
                     #:categories (list "ConsoleOnly"
                                        "System")
                     #:mime-type (list "application/x-shellscript"
                                       "text/english"
                                       "text/plain"
                                       "text/troff"
                                       "text/x-c"
                                       "text/x-c++"
                                       "text/x-c++hdr"
                                       "text/x-c++src"
                                       "text/x-chdr"
                                       "text/x-csrc"
                                       "text/x-diff"
                                       "text/x-java"
                                       "text/x-lisp"
                                       "text/x-makefile"
                                       "text/x-moc"
                                       "text/x-pascal"
                                       "text/x-scheme"
                                       "text/x-shellscript"
                                       "text/x-tcl"
                                       "text/x-tex"
                                       "text/x-tex"
                                       "text/xml"))
                   (mkdir-p tools)
                   ;; I don't think this should be necessary, but it seems to be
                   (copy-file
                     #$(plain-file "plugin-loader.kak" "\
eval %sh{
  IFS=:
  for dir in $GUIX_KAK_PLUGINS_PATH; do
    printf 'try %%{ source %s }\\n' $dir/*/*.kak
  done
}")
                     (string-append tools "/plugin-loader.kak"))))))
      (native-search-paths
       (list (search-path-specification
              (variable "GUIX_KAK_PLUGINS_PATH")
              (files '("share/kak/autoload/plugins"))))))))
