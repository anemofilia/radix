(define-module (radix packages pulseaudio)
  #:use-module (guix build utils)
  #:use-module (guix build-system cmake)
  #:use-module (guix gexp)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (gnu packages haskell-xyz)
  #:use-module (gnu packages ncurses)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages pulseaudio)
  #:use-module ((radix packages) #:prefix radix:))

(define-public ncpamixer
  (package
    (name "ncpamixer")
    (version "1.3.7")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/fulhax/ncpamixer/")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1ix4cja3yka8ah5s1ygd0ghf53jllphnjxjnkivvx72jii4b778q"))
       (patches
         (parameterize ((%patch-path radix:%patch-path))
           (search-patches "ncpamixer-remove-dynamic-download.patch")))))
    (build-system cmake-build-system)
    (arguments
     (list #:tests? #f
           #:configure-flags
           #~(list (string-append "PREFIX=" #$output)
                   "RELEASE=1"
                   "USE_WIDE=1")
           #:phases
           #~(modify-phases %standard-phases
               (add-after 'unpack 'patch-cmakelists
                 (lambda* (#:key inputs #:allow-other-keys)
                   (substitute* "src/CMakeLists.txt"
                     (("PandocMan")
                      (search-input-file inputs "PandocMan.cmake")))))
               (add-before 'configure 'chdir-src
                 (lambda _ (chdir "src")))
               (add-after 'install 'install-desktop-file
                 (lambda _
                   (let* ((out #$output)
                          (applications (string-append out "/share/applications"))
                          (ncpamixer-bin (string-append out "/bin/ncpamixer")))
                     (make-desktop-entry-file
                       (string-append applications "/ncpamixer.desktop")
                       #:type "Application"
                       #:name "ncpamixer"
                       #:comment "Launches the ncpamixer audio manager."
                       #:icon "utilities-terminal"
                       #:terminal #t
                       #:try-exec ncpamixer-bin
                       #:exec ncpamixer-bin
                       #:categories (list "ConsoleOnly" "System"))))))))
    (native-inputs
     (list (origin
             (method git-fetch)
             (uri (git-reference
                    (url "https://github.com/rnpgp/cmake-modules")
                    (commit "d5d76b2318f029f19b03ffb20bdd0b2d077d14bf")))
             (sha256
              (base32 "0ss6fbiy8f9jbpc2vyn21d6iyindf474fig2b8x6w6qxg3kmgvn3")))
           pandoc
           pkg-config))
   (inputs
    (list ncurses
          pulseaudio))
    (home-page "https://github.com/fulhax/ncpamixer")
    (synopsis "Ncurses PulseAudio Mixer")
    (description "An ncurses mixer for PulseAudio inspired by pavucontrol.")
    (license license:expat)))
