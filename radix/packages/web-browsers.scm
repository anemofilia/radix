(define-module (radix packages web-browsers)
  #:use-module (ice-9 match)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (gnu packages chromium))

(define-public ungoogled-chromium-minimal/wayland
  (package/inherit ungoogled-chromium/wayland
    (name "ungoogled-chromium-minimal-wayland")
    (inputs
      (filter (match-lambda
               ((package-name _)
                (not (member package-name
                             `("libx11"
                               "libxcb"
                               "libxcomposite"
                               "libxcursor"
                               "libxdamage"
                               "libxext"
                               "libxfixes"
                               "libxi"
                               "libxrandr"
                               "libxrender"
                               "libxscrnsaver"
                               "pulseaudio")))))
             (package-inputs ungoogled-chromium/wayland)))))
