(define-module (radix packages cpp)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages check)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system cmake)
  #:use-module ((guix licenses) #:prefix license:))

;; Special thanks to Murilo (6a046e63e2ccdd0d9af9364883597b4bb50e3ab9)
(define-public gsl-microsoft
  (package
    (name "gsl-microsoft")
    (version "4.0.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/Microsoft/GSL")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0dgb3rb6x2276d3v7x568m3zhqr67rhk8ynqgm3c304avnmcaw3i"))))
    (build-system cmake-build-system)
    (native-inputs (list pkg-config))
    (inputs (list googletest))
    (home-page "https://github.com/saitoha/libsixel")
    (synopsis "Guidelines Support Library")
    (description "The Guidelines Support Library (GSL) contains functions and
types that are suggested for use by the C++ Core Guidelines maintained by the
Standard C++ Foundation. This repo contains Microsoft's implementation of GSL.")
    (license license:expat)))
