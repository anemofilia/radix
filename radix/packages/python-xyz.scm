(define-module (radix packages python-xyz)
  #:use-module (guix build-system pyproject)
  #:use-module (guix build-system python)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (gnu packages check)
  #:use-module (gnu packages python-build)
  #:use-module (gnu packages python-science)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages sphinx)
  #:use-module (gnu packages xdisorg))

(define-public python-pytmx
  (package
    (name "python-pytmx")
    (version "3.32")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "PyTMX" version))
       (sha256
        (base32 "1jh9b0pjqbjdv72v5047p5d769ic084g013njvky0zcfiwrxi3w5"))))
    (build-system pyproject-build-system)
    (native-inputs (list python-setuptools python-wheel))
    (home-page "")
    (synopsis "Loads tiled tmx maps")
    (description "Loads tiled tmx maps.")
    (license license:lgpl3)))

(define-public python-pyscroll
  (package
    (name "python-pyscroll")
    (version "2.31")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "pyscroll" version))
       (sha256
        (base32 "0w3c58mkkbsyvx9w9hwdizk20pbds800m7v9vg49ydw440dha0hr"))))
    (build-system pyproject-build-system)
    (native-inputs (list python-setuptools python-wheel))
    (home-page "https://github.com/bitcraft/pyscroll")
    (synopsis "Fast scrolling maps library for pygame")
    (description "Fast scrolling maps library for pygame.")
    (license license:gpl3)))

(define-public python-neteria
  (package
    (name "python-neteria")
    (version "1.0.2")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "neteria" version))
       (sha256
        (base32 "1azlix80a6vns2i3z0bdbqk32kx8s2gjh2nvshab235fd9h85yv7"))))
    (build-system pyproject-build-system)
    (propagated-inputs (list python-rsa))
    (native-inputs (list python-setuptools python-wheel))
    (home-page "http://www.neteria.org")
    (synopsis "A simple game networking library.")
    (description "This package provides a simple game networking library.")
    (license license:gpl2)))

;; currently broken
(define-public python-ninja
  (package
    (name "python-ninja")
    (version "1.11.1.3")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "ninja" version))
       (sha256
        (base32 "0xkhi5sb2043in2fkqmnk1kzbj3cslma6h1yn0sidbbyklp0vypd"))))
    (build-system pyproject-build-system)
    (arguments
      (list #:phases
            #~(modify-phases %standard-phases
                (add-after 'unpack 'fix-bindir
                  (lambda _
                    (substitute* "src/ninja/__init__.py"
                      (("BIN_DIR = .*" prefix)
                       (string-append "BIN_DIR = \"" #$output "/bin" "\""))))))))
    (native-inputs (list python-coverage python-flit-core
                         python-importlib-metadata python-pytest
                         python-pytest-cov python-scikit-build-core))
    (home-page "https://github.com/scikit-build/ninja-python-distributions")
    (synopsis "Ninja is a small build system with a focus on speed")
    (description "Ninja is a small build system with a focus on speed.")
    (license license:asl2.0)))

(define-public python-meson
  (package
    (name "python-meson")
    (version "1.6.1")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "meson" version))
       (sha256
        (base32 "1pk4fjcxnsgjzmmx39ih9s0c0mzmiryk7lvzwsz8pm96dkmlkjhy"))))
    (arguments
      (list #:tests? #f))
    (build-system pyproject-build-system)
    (native-inputs (list python-setuptools python-wheel))
    (home-page "https://mesonbuild.com")
    (synopsis "A high performance build system")
    (description "This package provides a high performance build system.")
    (license license:asl2.0)))

(define-public python-pygame-ce-2.3
  (package
    (name "python-pygame-ce")
    (version "2.3.2")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "pygame-ce" version))
       (sha256
        (base32 "0gajfa7hsg5gmc4zi51h591jhawnsj57va86m9r8i263n0z86m1w"))))
    (build-system pyproject-build-system)
    (native-inputs (list python-cython python-meson python-meson-python
                         python-ninja python-sphinx))
    (home-page "https://github.com/pygame-community/pygame-ce")
    (synopsis "Python Game Development")
    (description "Python Game Development.")
    (license license:lgpl2.1+)))

(define-public python-pygame-ce
  (package
    (name "python-pygame-ce")
    (version "2.5.2")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "pygame_ce" version))
       (sha256
        (base32 "1nfaffbcd4y4smkn3l9jqry0fj8ndx86a0dm3a7vn4yh0pgjjrsc"))))
    (build-system pyproject-build-system)
    (native-inputs (list python-cython python-meson python-meson-python
                         python-ninja python-sphinx))
    (home-page "https://github.com/pygame-community/pygame-ce")
    (synopsis "Python Game Development")
    (description "Python Game Development.")
    (license #f)))

(define-public python-pygame-menu-ce
  (package
    (name "python-pygame-menu-ce")
    (version "4.4.3")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "pygame-menu-ce" version))
       (sha256
        (base32 "1f32mlc2lh18cp2fax01h3s3fdasfmz2jj18an7y5r9d9c30ypm7"))))
    (build-system pyproject-build-system)
    (propagated-inputs (list python-pygame-ce python-pyperclip
                             python-typing-extensions))
    (native-inputs (list python-nose2 python-setuptools python-wheel))
    (home-page "https://pygame-menu.readthedocs.io")
    (synopsis "A menu for pygame-ce. Simple, and easy to use")
    (description
     "This package provides a menu for pygame-ce.  Simple, and easy to use.")
    (license license:expat)))
