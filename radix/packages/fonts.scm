(define-module (radix packages fonts)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system font)
  #:use-module (gnu packages compression)
  #:use-module ((guix licenses)
                #:prefix license:))

(define-public font-meslo-lg
  (package
    (name "font-meslo-lg")
    (version "1.2.1")
    (source (origin
              (file-name "font-meslo-lg")
              (method url-fetch/zipbomb)
              (uri (string-append
                    "https://github.com/andreberg/Meslo-Font/raw/master/"
                    "dist/v" version "Meslo%20LG%20v" version ".zip"))
              (sha256
               (base32
                "1l08mxlzaz3i5bamnfr49s2k4k23vdm64b8nz2ha33ysimkbgg6h"))))
    (build-system font-build-system)
    (native-inputs (list unzip))
    (home-page "https://github.com/andreberg/Meslo-Font")
    (synopsis "Font for dyslexics and high readability")
    (description
     "Meslo LG is a customized version of Apple’s Menlo-Regular font
(which is a customized Bitstream Vera Sans Mono).")
    (license license:silofl1.1)))

(define-public font-meslo-lg-dz
  (package
    (inherit font-meslo-lg)
    (name "font-meslo-lg-dz")
    (version (package-version font-meslo-lg))
    (source (origin
              (file-name "font-meslo-lg-dz")
              (method url-fetch/zipbomb)
              (uri (string-append
                    "https://github.com/andreberg/Meslo-Font/raw/master/"
                    "dist/v" version "Meslo%20LG%20DZ%20v" version ".zip"))
              (sha256
               (base32
                "0lnbkrvcpgz9chnvix79j6fiz36wj6n46brb7b1746182rl1l875"))))))
