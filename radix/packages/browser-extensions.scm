(define-module (radix packages browser-extensions)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system copy)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu build chromium-extension))

(define darkreader
  (package
   (name "darkreader")
   (version "4.9.65")
   (home-page "https://github.com/darkreader/darkreader")
   (source (origin
            (method url-fetch/zipbomb)
            (uri (string-append home-page "/releases/download/"
                                "v" version "/" name "-chrome.zip"))
            (sha256
             (base32
              "12nbd1b33r0jpgdk9zmngria2f7naxm4ahk23chmqja4bsisgd2v"))))
   (build-system copy-build-system)
   (synopsis "Dark Reader analyzes web pages and aims to reduce eyestrain while
browsing the web.")
   (description "Dark Reader is an open-source MIT-licensed browser extension
designed to analyze web pages. Dark Reader will generate a dark mode that
aims to reduce the eyestrain of the user. Dark Reader is feature-rich
and is customizable in many ways throughout the UI.")
   (license license:expat)))

(define-public darkreader/chromium
  (make-chromium-extension darkreader))
