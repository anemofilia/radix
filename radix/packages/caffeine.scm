(define-module (radix packages caffeine)
  #:use-module (gnu packages)
  #:use-module (gnu packages audio)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages man)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-build)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages xfce)
  #:use-module (guix build-system meson)
  #:use-module (guix build-system python)
  #:use-module (guix build-system pyproject)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix gexp)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module ((radix packages) #:prefix radix:))

(define-public python-ewmh
  (package
    (name "python-ewmh")
    (version "0.1.6")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "ewmh" version))
       (sha256
        (base32 "0g9l14my3v8zlgq1yd8wh5gpara0qcapsfmvg7lq2lapglzhjsy5"))))
    (build-system pyproject-build-system)
    (propagated-inputs (list python-xlib))
    (native-inputs (list python-setuptools python-wheel))
    (home-page "https://github.com/parkouss/pyewmh")
    (synopsis
     "python implementation of Extended Window Manager Hints, based on Xlib")
    (description
     "python implementation of Extended Window Manager Hints, based on Xlib.")
    (license license:lgpl3)))

(define-public caffeine-ng
  (package
    (name "caffeine-ng")
    (version "4.2.0")
    (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url "https://codeberg.org/WhyNotHugo/caffeine-ng")
                  (commit (string-append "v" version))))
            (file-name (git-file-name name version))
            (sha256
             (base32
              "0i2pp9fx37cqa7h5bc7vfnif109dd485k29bhlq04rmskx2wp35r"))
            (patches
             (parameterize ((%patch-path radix:%patch-path))
               (search-patches "caffeine-install.patch")))))
    (build-system meson-build-system)
    (arguments
      (list #:configure-flags
            #~(list (string-append "-Dbindir=" #$output "/bin"))
            #:phases
            #~(modify-phases %standard-phases
                (add-after 'unpack 'inform-version
                  (lambda _
                    (substitute* "meson.build"
                      (("run_command.*") (string-append "'" #$version "',"))))))))
    (inputs
      (list dbus
            gtk+
            libnotify
            libappindicator
            python-3.10
            python-click
            python-dbus
            python-ewmh
            python-pygobject
            python-pulsectl
            python-setproctitle
            xfconf
            xdg-utils))
    (native-inputs
      (list cmake-3.30
            pkg-config
            scdoc))
    (home-page "https://codeberg.org/WhyNotHugo/caffeine-ng")
    (synopsis "Tray bar application able to temporarily inhibits the screensaver
and sleep mode.")
    (description "Caffeine is a little daemon that sits in your systray, and
prevents the screensaver from showing up, or the system from going to sleep. It
does so when an application is fullscreened (eg: youtube), or when you click on
the systray icon (which you can do, when, eg: reading).")
    (license license:gpl3)))
