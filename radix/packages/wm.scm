(define-module (radix packages wm)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages crates-check)
  #:use-module (gnu packages crates-compression)
  #:use-module (gnu packages crates-graphics)
  #:use-module (gnu packages crates-io)
  #:use-module (gnu packages crates-windows)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages guile)
  #:use-module (gnu packages guile-xyz)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages man)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages web)
  #:use-module (gnu packages wm)
  #:use-module (gnu packages zig)
  #:use-module (gnu packages zig-xyz)
  #:use-module (gnu packages)
  #:use-module (guix build-system cargo)
  #:use-module (guix build-system copy)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system zig)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (guix transformations)
  #:use-module (guix utils)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (radix packages crates-io))

(define-public awesome-next
  ((options->transformation
     `((with-branch  . "awesome=master")
       (with-git-url . "awesome=https://github.com/awesomeWM/awesome")))
   awesome))

(define-public rivercarro
  (package
   (name "rivercarro")
   (version "0.5.0")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://git.sr.ht/~novakane/rivercarro")
           (commit (string-append "v" version))
           (recursive? #t)))
     (file-name (git-file-name name version))
     (sha256
      (base32 "0vfxig91lwxibal30r7j5mrr2yp3d7i6bz5v67zanxj3j8yzlrcj"))))
   (build-system zig-build-system)
   (arguments
    (list #:tests? #f
          #:install-source? #f
          #:zig-release-type "safe"
          #:phases #~(modify-phases %standard-phases
                       (delete 'validate-runpath)
                       (add-after 'install 'install-documentation
                         (lambda* (#:key inputs outputs #:allow-other-keys)
                           (let* ((out (assoc-ref outputs "out"))
                                  (man (string-append out "/share/man/man1")))
                             (install-file "doc/rivercarro.1" man)))))))
   (native-inputs
    (list pkg-config
          zig-wayland))
   (home-page "https://git.sr.ht/~novakane/rivercarro")
   (synopsis "A slightly modified version of rivertile layout generator for river")
   (description
    "A modified version of rivertile which adds: monocle layout, gaps rather
than padding, gap size modification at run time, and smart gaps.")
   (license license:gpl3+)))

(define-public riverguile
  (package
    (name "riverguile")
    (version "0.1.0")
    (source
     (origin
      (method git-fetch)
      (uri (git-reference
            (url "https://git.sr.ht/~leon_plickat/riverguile")
            (commit (string-append "v" version))
            (recursive? #t)))
      (file-name (git-file-name name version))
      (sha256
       (base32 "1m14pfk1mxkijmpcs0gqlpvyp6bzc214fdn8aq0r5llg5v0wkidz"))))
   (build-system gnu-build-system)
   (arguments
     (list #:tests? #f
           #:make-flags
           #~(list (string-append "CC=" #$(cc-for-target))
                   (string-append "PREFIX=" (assoc-ref %outputs "out")))
           #:phases
           #~(modify-phases %standard-phases
               (delete 'check)
               (delete 'configure))))
   (native-inputs
     (list guile-3.0
           guile-config
           pkg-config
           wayland))
   (home-page "https://git.sr.ht/~leon_plickat/riverguile")
   (synopsis "Scripting layer for the river Wayland server using Guile Scheme.")
   (description "Scripting layer for the river Wayland server using Guile Scheme.
Send commands to river and install handlers for various events, including layout
demands.
The ultimate aim of riverguile is to allow comfortable scripting in the context
of desktop operations, including window management.")
   (license license:gpl3)))

(define-public river-bnf
  (package
    (name "river-bnf")
    (version "0.1.0")
    (source
     (origin
      (method git-fetch)
      (uri (git-reference
            (url "https://git.sr.ht/~leon_plickat/river-bnf")
            (commit "bb8ded380ed5d539777533065b4fd33646ad5603")
            (recursive? #t)))
      (file-name (git-file-name name version))
      (sha256
       (base32 "1iaxfj1n0pvw483385aqnc0493m78s093svw758az0lbfnvlsvxf"))))
   (build-system gnu-build-system)
   (arguments
     (list #:tests? #f
           #:make-flags
           #~(list (string-append "CC=" #$(cc-for-target))
                   (string-append "PREFIX=" (assoc-ref %outputs "out")))
           #:phases
           #~(modify-phases %standard-phases
               (delete 'configure))))
   (native-inputs
     (list guile-3.0
           guile-config
           pkg-config
           wayland))
   (home-page "https://git.sr.ht/~leon_plickat/river-bnf")
   (synopsis "Switch back'n'forth between river tags.")
   (description "A tool for the river Wayland compositor which implements the
\"back-and-forth\" functionality for tag switching which is common in similar
desktops. Trying to focus a tag will focus it, unless it is already focused in
which case the previously focused tags are focused again.")
   (license license:gpl3)))

(define-public river-bedload
  (package
    (name "river-bedload")
    (version "0.1.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://git.sr.ht/~novakane/river-bedload")
              (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
         (base32 "146lm94c92y5xivcpz749bvbg599phcsp9nqamqlvq3jlms4bk00"))))
   (build-system zig-build-system)
   (arguments
     (list #:tests? #f
           #:install-source? #f
           #:phases
           #~(modify-phases %standard-phases
               (add-after 'unpack 'fix-build.zig.zon
                 (lambda* (#:key inputs #:allow-other-keys)
                   (substitute* "build.zig.zon"
                     (("https://.*.tar.gz")
                      (assoc-ref inputs "zig-wayland"))))))))
   (native-inputs
     (list pkg-config
           wayland
           zig-wayland))
   (home-page "https://git.sr.ht/~novakane/river-bedload")
   (synopsis "Display information about river in json in the STDOUT.")
   (description "Display information about river in json in the STDOUT.")
   (license license:gpl3)))

(define-public waybar-sans-elogind
  ((package-input-rewriting
    `((,wireplumber . ,wireplumber-minimal)))
   waybar))

(define-public river-minimal
  (package/inherit river
    (name "river-minimal")
    (arguments
      (substitute-keyword-arguments (package-arguments river)
        ;; Please, no xwayland here, thank you very much
        ((#:zig-build-flags flags #~'()) #~'())))
    (native-inputs
      (modify-inputs (package-native-inputs river)
        (delete "libxkbcommon")))))

(define-public lswt
  (package
    (name "lswt")
    (version "2.0.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://git.sr.ht/~leon_plickat/lswt")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0g9mw4w2ksmbjlwj2s7gp291rjwrgn0snviv95xxw3pcdhizlczj"))))
    (build-system gnu-build-system)
    (arguments
      (list #:tests? #f
            #:make-flags
            #~(list (string-append "CC=" #$(cc-for-target))
                    (string-append "PREFIX=" #$output))
            #:phases
            #~(modify-phases %standard-phases
                (delete 'configure))))
    (native-inputs
      (list pkg-config
            wayland
            wayland-protocols))
    (home-page "https://git.sr.ht/~leon_plickat/lswt")
    (synopsis "A tool for listing wayland toplevels.")
    (description "lswt is a tool for listing wayland toplevels.")
    (license license:gpl3)))

(define-public riverprop
  (package
    (name "riverprop")
    (version "0.0.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://codeberg.org/anemofilia/riverprop")
              (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
         (base32 "1lp3grgcvcbzkd1hl3c426y9yd8vx3mrmx3z33g33509jw4whnw4"))))
    (build-system copy-build-system)
    (arguments
     (list #:install-plan
           #~'(("riverprop" "bin/"))
           #:phases
           #~(modify-phases %standard-phases
               (add-after 'install 'patch-sh
                 (lambda* (#:key inputs #:allow-other-keys)
                   (let ((jq (assoc-ref inputs "jq"))
                         (lswt (assoc-ref inputs "lswt")))
                     (substitute* (string-append #$output "/bin/riverprop")
                       (("jq") (string-append jq "/bin/jq"))
                       (("lswt") (string-append lswt "/bin/lswt")))))))))
    (home-page "https://codeberg.org/anemofilia/riverprop")
    (inputs (list jq lswt))
    (synopsis "Get information about the currently active wayland toplevels.")
    (description "riverprop is a simple tool to get information about the
currently active wayland topolevels.")
    (license license:gpl3)))

(define-public ristate
  (let ((commit "92e989f26cadac69af1208163733e73b4cf447da")
        (revision "1"))
    (package
      (name "ristate")
      (version (git-version "0.1.0" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
                (url "https://gitlab.com/snakedye/ristate")
                (commit commit)))
         (file-name (git-file-name name version))
         (sha256
           (base32 "04vyq19hf5bymgr49gy54rlf1f8i075dzsny0p82avd43vnlgjga"))))
      (build-system cargo-build-system)
      (arguments
       `(#:cargo-inputs (("rust-serde" ,rust-serde-1)
                         ("rust-serde-json" ,rust-serde-json-1)
                         ("rust-wayland-client" ,rust-wayland-client-0.29)
                         ("rust-wayland-commons" ,rust-wayland-commons-0.29)
                         ("rust-wayland-protocols" ,rust-wayland-protocols-0.29)
                         ("rust-wayland-scanner" ,rust-wayland-scanner-0.29))))
      (home-page "https://gitlab.com/snakedye/ristate")
      (synopsis "A river-status client written in rust.")
      (description "Ristate provides a river status client. This client is useful
  if you want to have a module for somethig like eww or waybar with status
  information like the window title, the focused tag and the tag of views.")
      (license license:expat))))

(define-public swww
  (package
    (name "swww")
    (version "0.9.5")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
              (url "https://github.com/LGFae/swww")
              (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1ivfaw1ff5z68cw15s5qgshk8gqdx9fjslgvfr9xnn9c28gbvp4m"))
       (modules '((guix build utils)))
       (snippet
        '(substitute* "utils/Cargo.toml"
           (("\"=([[:digit:]]+(\\.[[:digit:]]+)*)" _ version)
            (string-append "\"^" version))))))
    (build-system cargo-build-system)
    (arguments
     (list #:install-source? #f
           #:cargo-inputs
           `(("rust-assert-cmd" ,rust-assert-cmd-2)
             ("rust-bitcode" ,rust-bitcode-0.6)
             ("rust-clap" ,rust-clap-4)
             ("rust-clap-complete" ,rust-clap-complete-4)
             ("rust-fast-image-resize" ,rust-fast-image-resize-3)
             ("rust-fastrand" ,rust-fastrand-2)
             ("rust-image" ,rust-image-0.25)
             ("rust-keyframe" ,rust-keyframe-1)
             ("rust-sd-notify" ,rust-sd-notify-0.4)
             ("rust-simplelog" ,rust-simplelog-0.12)
             ("rust-spin-sleep" ,rust-spin-sleep-1)
             ("rust-wayland-client" ,rust-wayland-client-0.31)
             ("rust-wayland-protocols" ,rust-wayland-protocols-0.31)
             ("rust-wayland-protocols-wlr" ,rust-wayland-protocols-wlr-0.2))
          #:cargo-development-inputs
          `(("rust-rand" ,rust-rand-0.8)
            ("rust-assert-cmd" ,rust-assert-cmd-2)
            ("rust-criterion" ,rust-criterion-0.5))
          #:phases
          #~(modify-phases %standard-phases
             (add-after 'unpack 'disable-internet-requirement
               (lambda _
                 (substitute* '("daemon/Cargo.toml"
                                "utils/Cargo.toml")
                   (("git =.*, d") "version = \"0.6\", d"))))
              (add-before 'build 'build-documentation
                (lambda* (#:key inputs #:allow-other-keys)
                  (invoke "doc/gen.sh")))
              (replace 'install
                (lambda* (#:key outputs #:allow-other-keys)
                  (let* ((out (assoc-ref outputs "out"))
                         (bin (string-append out "/bin"))
                         (share (string-append out "/share"))
                         (man1 (string-append share "/man/man1"))
                         (swww (car (find-files "target" "^swww$")))
                         (swww-daemon (car (find-files "target" "^swww-daemon$")))
                         (bash-completions-dir
                          (string-append share "/bash-completion/completions"))
                         (zsh-completions-dir
                          (string-append share "/zsh/site-functions"))
                         (fish-completions-dir
                          (string-append share "/fish/vendor_completions.d"))
                         (elvish-completions-dir
                          (string-append share "/elvish/lib")))
                    (install-file swww bin)
                    (install-file swww-daemon bin)
                    (copy-recursively "doc/generated" man1)
                    (install-file "completions/swww.bash" bash-completions-dir)
                    (install-file "completions/_swww" zsh-completions-dir)
                    (install-file "completions/swww.fish" fish-completions-dir)
                    (install-file "completions/swww.elv" elvish-completions-dir))))
              (add-after 'install 'wrap-binaries
               (lambda* (#:key outputs inputs #:allow-other-keys)
                 (let ((out (assoc-ref outputs "out"))
                       (lz4 (assoc-ref inputs "lz4")))
                   (wrap-program (string-append out "/bin/swww")
                     `("PATH" prefix (,(string-append lz4 "/bin"))))
                   (wrap-program (string-append out "/bin/swww-daemon")
                     `("PATH" prefix (,(string-append lz4 "/bin"))))))))))
    (native-inputs (list scdoc))
    (inputs (list bash-minimal lz4 pkg-config))
    (home-page "https://github.com/LGFae/swww")
    (synopsis "Efficient animated wallpaper daemon for wayland controlled at
runtime.")
    (description "A Solution to your Wayland Wallpaper Woes (swww).  It uses
minimal resources and provides animations for switching between backgrounds.")
    (license license:gpl3)))
