;;; Copyright © 2024 Luis Guilherme Coelho <lgcoelho@disroot.org>
;;; Copyright © 2024 Murilo <murilo@disroot.org>

(define-module (radix packages games)
  #:use-module (gnu packages)
  #:use-module (gnu packages audio)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages commencement)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages image)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages python-web)
  #:use-module (gnu packages sdl)
  #:use-module (gnu packages serialization)
  #:use-module (gnu packages upnp)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system copy)
  #:use-module (guix build-system pyproject)
  #:use-module (guix gexp)
  #:use-module (ice-9 pretty-print)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix utils)
  #:use-module (radix packages python-xyz)
  #:use-module ((radix packages) #:prefix radix:))

(define-public srb2-assets
  (package
    (name "srb2-assets")
    (version "2.2.13")
    (source
      (origin
        (method git-fetch/lfs)
        (uri (git-reference
              (url "https://git.do.srb2.org/STJr/srb2assets-public")
              (commit (string-append "SRB2_release_" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0604msd1zas02l6h739f39vpcpy7xmhfpp7bc4k8d33fk7jwwyrr"))))
   (arguments
     (list #:install-plan
           #~`(("." "share/srb2"))))
   (build-system copy-build-system)
   (home-page "https://git.do.srb2.org/STJr/srb2assets-public")
   (synopsis "Assets for SRB2")
   (description "Assets for SRB2")
   (license license:gpl2)))

(define-public srb2
  (package
    (name "srb2")
    (version "2.2.13")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/STJr/SRB2")
                    (commit (string-append "SRB2_release_" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0zv1r4hl2l979qpr9zwxg76wnwd6jfzrfypqs9z6wrgv5j628a9r"))
             (patches
               (parameterize ((%patch-path radix:%patch-path))
                 (search-patches
                   "srb2-cmake.patch"
                   "srb2-thirdparty.patch")))))
    (build-system cmake-build-system)
    (arguments
      (list #:configure-flags
            #~(list (string-append "-DSRB2_ASSET_DIRECTORY="
                                   #$(this-package-input "srb2-assets")
                                   "/share/srb2")
                    (string-append "-DGME_INCLUDE_DIR="
                                   #$(this-package-native-input "libgme")
                                   "/include")
                    (string-append "-DOPENMPT_INCLUDE_DIR="
                                   #$(this-package-native-input "libopenmpt")
                                   "/include")
                    (string-append "-DSDL2_MIXER_INCLUDE_DIR="
                                   #$(this-package-native-input "sdl2-mixer")
                                   "/include/SDL2")
                    (string-append "-DSDL2_INCLUDE_DIR="
                                   #$(this-package-native-input "sdl2")
                                   "/include/SDL2"))
            #:phases
            #~(modify-phases %standard-phases
                (delete 'check)
                (replace 'install
                  (lambda _
                    (let* ((bin (string-append #$output "/bin"))
                           (apps (string-append #$output "/share/applications"))
                           (srb2-assets #$(this-package-input "srb2-assets"))
                           (share-srb2 (string-append srb2-assets "/share/srb2")))
                      (install-file "bin/lsdlsrb2" bin)
                      (chdir bin)
                      (rename-file "lsdlsrb2" "srb2")
                      (wrap-program (string-append bin "/srb2")
                        `("SRB2WADDIR" prefix (,share-srb2)))
                      (mkdir-p apps)
                      (make-desktop-entry-file
                        (string-append apps "/srb2.desktop")
                        #:name "Sonic Robo Blast 2"
                        #:exec (string-append #$output "/bin/srb2")
                        #:try-exec (string-append #$output "/bin/srb2")
                        #:comment `((#f ,#$(package-synopsis this-package))))))))))
    (native-inputs
      (list libgme
            curl
            libopenmpt
            libpng
            libupnp
            pkg-config
            sdl2
            sdl2-mixer
            zlib))
    (inputs
      (list bash-minimal
            srb2-assets))
    (home-page "https://github.com/STJr/SRB2")
    (synopsis "Sonic Robo Blast 2 is a 3D Sonic the Hedgehog fangame based on a
modified version of Doom Legacy.")
    (description "Sonic Robo Blast 2 is a 3D Sonic the Hedgehog fangame based on
a modified version of Doom Legacy.")
    (license license:gpl2)))
