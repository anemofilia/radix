(define-module (radix packages fish-xyz)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages rust-apps)
  #:use-module (gnu packages shells)
  #:use-module (gnu packages terminals)
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix build-system copy)
  #:use-module ((guix licenses)
                #:prefix license:))

(define-public fish-autopair
  (package
    (name "fish-autopair")
    (version "1.0.4")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/jorgebucaran/autopair.fish")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0mfx43n3ngbmyfp4a4m9a04gcgwlak6f9myx2089bhp5qkrkanmk"))))
    (build-system copy-build-system)
    (arguments
     (list #:install-plan #~'(("conf.d" "share/fish/")
                              ("functions" "share/fish/"))))
    (home-page "https://github.com/jorgebucaran/autopair.fish")
    (synopsis "Auto-complete matching pairs for the Fish shell.")
    (description "This package aims to provide auto-complete matching pairs
for the Fish shell.")
    (license license:expat)))

(define-public fish-fzf
  (package
    (name "fish-fzf")
    (version "10.3")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/PatrickF1/fzf.fish")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1hqqppna8iwjnm8135qdjbd093583qd2kbq8pj507zpb1wn9ihjg"))))
    (build-system copy-build-system)
    (arguments
     (list #:install-plan #~'(("completions" "share/fish/")
                              ("conf.d" "share/fish/")
                              ("functions" "share/fish/"))))
    (propagated-inputs (list bat fd fzf))
    (home-page "https://github.com/PatrickF1/fzf.fish")
    (synopsis "Mnemonic key bindings for using fzf within the Fish shell.")
    (description "This package aims to augment your Fish shell with mnemonic
key bindings to efficiently find what you need using fzf.")
    (license license:expat)))

(define-public fish-bang-bang
  (package
    (name "fish-bang-bang")
    (version "1.0.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/oh-my-fish/plugin-bang-bang")
             (commit "ec991b80ba7d4dda7a962167b036efc5c2d79419")))
       (sha256
        (base32 "1bf61f6h5p7mc0schwbd693cafp1vcjz2f7pzy6gn33nafsc5wx0"))))
    (build-system copy-build-system)
    (arguments
     (list #:install-plan #~'(("conf.d" "share/fish/")
                              ("functions" "share/fish/"))))
    (home-page "https://github.com/oh-my-fish/plugin-bang-bang")
    (synopsis "Bash style history substitution for the Fish shell.")
    (description "This package aims to provide Bash style history substitution
for the Fish shell.")
    (license license:expat)))

(define-public fish-puffer
  (package
    (name "fish-puffer")
    (version "1.0.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/nickeb96/puffer-fish.git")
             (commit (string-append "v" version))))
       (sha256
        (base32 "06g8pv68b0vyhhqzj469i9rcics67cq1kbhb8946azjb8f7rhy6s"))))
    (build-system copy-build-system)
    (arguments
     (list #:install-plan #~'(("functions" "share/fish/")
                              ("conf.d" "share/fish/"))))
    (home-page "https://github.com/nickeb96/puffer-fish.git")
    (synopsis "Text Expansions for the Fish shell.")
    (description "This plugin aims to provide text expansions for the Fish
shell.")
    (license license:expat)))

(define-public fish-functional
  (package
    (name "fish-functional")
    (version "1.0.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/oh-my-fish/plugin-functional")
             (commit "0d3ab3169ff489714761c7a9ad21e268914afa31")))
       (sha256
        (base32 "0vnq4kpilg0z470d7782pp8wdj57cfkfad27d6mcylwl8n7wnhrn"))))
    (build-system copy-build-system)
    (arguments
     (list #:install-plan #~'(("functions" "share/fish/"))))
    (home-page "https://github.com/oh-my-fish/plugin-functional")
    (synopsis "Higher order functions for the Fish shell.")
    (description "This plugin aims to provide higher order functions for the
Fish shell.")
    (license license:expat)))

(define-public fish-done
  (package
    (name "fish-done")
    (version "1.19.3")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/franciscolourenco/done")
             (commit version)))
       (sha256
        (base32 "12l7m08bp8vfhl8dmi0bfpvx86i344zbg03v2bc7wfhm20li3hhc"))))
    (build-system copy-build-system)
    (arguments
     (list #:install-plan #~'(("conf.d" "share/fish/"))))
    (propagated-inputs
     (list libnotify))
    (home-page "https://github.com/franciscolourenco/done")
    (synopsis "A fish-shell plugin to automatically receive notifications when
long processes finish.")
    (description "A fish-shell package to automatically receive notifications
when long processes finish.")
    (license license:expat)))

(define-public fish-sponge
  (package
    (name "fish-sponge")
    (version "1.1.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/meaningful-ooo/sponge")
             (commit version)))
       (sha256
        (base32 "0p4vk6pq858h2v39c41irrgw1fbbcs7gd9cdr9i9fd3d6i81kmri"))))
    (build-system copy-build-system)
    (arguments
     (list #:install-plan #~'(("conf.d" "share/fish/")
                              ("functions" "share/fish/"))))
    (home-page "https://github.com/meaningful-ooo/sponge")
    (synopsis "A fish-shell plugin to clean fish history from typos
automatically.")
    (description "A fish-shell plugin to clean fish history from typos
automatically.")
    (license license:expat)))

(define-public fish-colored-man
  (let ((commit "1ad8fff696d48c8bf173aa98f9dff39d7916de0e")
        (revision "0"))
    (package
      (name "fish-colored-man")
      (version (git-version "0" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/decors/fish-colored-man")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32 "0l32a5bq3zqndl4ksy5iv988z2nv56a91244gh8mnrjv45wpi1ms"))))
      (build-system copy-build-system)
      (arguments
       (list #:install-plan #~'(("functions" "share/fish/"))))
      (home-page "https://github.com/decors/fish-colored-man")
      (synopsis "Color-enabled man pages plugin for fish-shell.")
      (description "This package provides color-enabled man pages plugin for
fish-shell.")
      (license license:expat))))

(define-public fish-expansion
  (package
    (name "fish-expansion")
    (version "1.0.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/oh-my-fish/plugin-expand")
             (commit (string-append "v" version))))
       (sha256
        (base32 "1k4bmk0c4kk42rr0x78vif02wq5cnwbyk9jgw8n846wvrnypm9bs"))))
    (build-system copy-build-system)
    (arguments
     (list #:install-plan #~'(("completions" "share/fish/")
                              ("functions" "share/fish/"))))
    (home-page "https://github.com/oh-my-fish/plugin-expand")
    (synopsis "Interactive word expansions in real-time for fish shell.")
    (description "This package provides interactive word expansions in
real-time for fish-shell.")
    (license license:expat)))
