(define-module (radix packages admin)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages imagemagick)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages python)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages xorg)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system copy)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module ((guix licenses)
                #:prefix license:))

(define-public zzz
  (let ((commit "329b891426efc093ad8c65e96364427c2fb25b54")
        (revision "0"))
    (package
      (name "zzz")
      (version (git-version "0.0.0" revision commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://github.com/void-linux/void-runit")
                      (commit commit)))
                (file-name (git-file-name name version))
                (sha256
                 (base32
                  "1lgf3maqw2j9hjzq0bkrvbl7vbh4zpppm9cdl9hvf54kbxcn2v9i"))))
      (build-system copy-build-system)
      (arguments
       (list #:install-plan #~'(("zzz" "bin/")
                                ("zzz.8" "share/man/man8/"))))
      (home-page "https://github.com/void-linux/void-runit")
      (synopsis "Simple script to suspend or hibernate your computer.")
      (description "Simple script to suspend or hibernate your computer.  It
suports hooks before and after suspending.")
      (license license:cc0))))

(define-public fastfetch
  (package
    (name "fastfetch")
    (version "2.16.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/fastfetch-cli/fastfetch")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "112dvfx7gvp6n20i1lkd0jbh897jf7bxjxq96bj4099j3x313y3m"))))
    (build-system cmake-build-system)
    (inputs (list dbus
                  glib
                  imagemagick
                  libxcb
                  mesa
                  python-3.10
                  wayland
                  zlib)) ;for imagemagick and an #ifdef
    (native-inputs (list pkg-config))
    (arguments
      (list #:tests? #f ; no test target
            #:phases #~(modify-phases %standard-phases
                         (add-after 'unpack 'fix-completion
                           (lambda _
                             (substitute* "completions/fastfetch.fish"
                               (("python3")
                                (string-append #$(this-package-input "python")
                                               "/bin/python3"))))))))
    (home-page "https://github.com/fastfetch-cli/fastfetch")
    (synopsis "Display system information in a stylized manner")
    (description
     "Fastfetch is a tool for fetching system information and displaying it in
a stylized way.  Fastfetch displays this information next to a logo of the
system distribution, akin to many similar tools.")
    (license license:expat)))
