(define-module (radix packages shells)
  #:use-module (gnu)
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:use-module (gnu packages shells))

(define-public dash-next
  (package
    (inherit dash)
    (arguments
     (list #:configure-flags #~(list "--with-libedit")
           #:phases #~(modify-phases %standard-phases
                        (add-after 'install 'create-/bin/sh
                          (lambda _
                            (symlink (string-append #$output "/bin/dash")
                                     (string-append #$output "/bin/sh")))))))
    (description
     "Dash is a POSIX-compliant @command{/bin/sh} implementation that aims to be
as small as possible, often without sacrificing speed.  It is faster than the
GNU Bourne-Again Shell (@command{bash}) at most scripted tasks.  Dash is a
direct descendant of NetBSD's Almquist Shell (@command{ash}). This package
provides, adtionally to @command{/bin/dash} a symlink to it under
@command{/bin/sh}.")))
