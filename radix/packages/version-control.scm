(define-module (radix packages version-control)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages crates-check)
  #:use-module (gnu packages crates-io)
  #:use-module (gnu packages crates-vcs)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages version-control)
  #:use-module (guix build-system cargo)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (radix packages crates-io))

(define-public gitu
  (package
    (name "gitu")
    (version "0.25.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "gitu" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "181fi69nri97ddwqdzk9wbwzcdn6ljgsmlnbl10c63zxj9amgwc5"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-arboard" ,rust-arboard-3)
                       ("rust-chrono" ,rust-chrono-0.4)
                       ("rust-clap" ,rust-clap-4)
                       ("rust-criterion" ,rust-criterion-0.5)
                       ("rust-crossterm" ,rust-crossterm-0.27)
                       ("rust-ctor" ,rust-ctor-0.2)
                       ("rust-derive-more" ,rust-derive-more-0.99)
                       ("rust-etcetera" ,rust-etcetera-0.8)
                       ("rust-figment" ,rust-figment-0.10)
                       ("rust-git-version" ,rust-git-version-0.3)
                       ("rust-git2" ,rust-git2-0.19)
                       ("rust-insta" ,rust-insta-1)
                       ("rust-itertools" ,rust-itertools-0.13)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-nom" ,rust-nom-7)
                       ("rust-pretty-assertions" ,rust-pretty-assertions-1)
                       ("rust-ratatui" ,rust-ratatui-0.27)
                       ("rust-regex" ,rust-regex-1)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-similar" ,rust-similar-2)
                       ("rust-simple-logging" ,rust-simple-logging-2)
                       ("rust-temp-dir" ,rust-temp-dir-0.1)
                       ("rust-toml" ,rust-toml-0.8)
                       ("rust-tree-sitter" ,rust-tree-sitter-0.20)
                       ("rust-tree-sitter-bash" ,rust-tree-sitter-bash-0.20)
                       ("rust-tree-sitter-c" ,rust-tree-sitter-c-0.20)
                       ("rust-tree-sitter-c-sharp" ,rust-tree-sitter-c-sharp-0.20)
                       ("rust-tree-sitter-cpp" ,rust-tree-sitter-cpp-0.20)
                       ("rust-tree-sitter-elixir" ,rust-tree-sitter-elixir-0.1)
                       ("rust-tree-sitter-go" ,rust-tree-sitter-go-0.20)
                       ("rust-tree-sitter-haskell" ,rust-tree-sitter-haskell-0.15)
                       ("rust-tree-sitter-highlight" ,rust-tree-sitter-highlight-0.20)
                       ("rust-tree-sitter-html" ,rust-tree-sitter-html-0.20)
                       ("rust-tree-sitter-java" ,rust-tree-sitter-java-0.20)
                       ("rust-tree-sitter-javascript" ,rust-tree-sitter-javascript-0.20)
                       ("rust-tree-sitter-json" ,rust-tree-sitter-json-0.20)
                       ("rust-tree-sitter-ocaml" ,rust-tree-sitter-ocaml-0.20)
                       ("rust-tree-sitter-php" ,rust-tree-sitter-php-0.20)
                       ("rust-tree-sitter-python" ,rust-tree-sitter-python-0.20)
                       ("rust-tree-sitter-ruby" ,rust-tree-sitter-ruby-0.20)
                       ("rust-tree-sitter-rust" ,rust-tree-sitter-rust-0.20)
                       ("rust-tree-sitter-scala" ,rust-tree-sitter-scala-0.20)
                       ("rust-tree-sitter-toml" ,rust-tree-sitter-toml-0.20)
                       ("rust-tree-sitter-typescript" ,rust-tree-sitter-typescript-0.20)
                       ("rust-tui-prompts" ,rust-tui-prompts-0.3)
                       ("rust-unicode-width" ,rust-unicode-width-0.1))))
    (native-inputs
      (list pkg-config))
    (inputs
      (list git libgit2-1.8 openssl zlib))
    (home-page "https://github.com/altsem/gitu")
    (synopsis "git client inspired by Magit")
    (description "This package provides a git client inspired by Magit.")
    (license license:expat)))
