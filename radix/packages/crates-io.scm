(define-module (radix packages crates-io)
  #:use-module (gnu packages crates-apple)
  #:use-module (gnu packages crates-check)
  #:use-module (gnu packages crates-compression)
  #:use-module (gnu packages crates-graphics)
  #:use-module (gnu packages crates-io)
  #:use-module (gnu packages crates-vcs)
  #:use-module (gnu packages crates-windows)
  #:use-module (guix build-system cargo)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module ((guix licenses) #:prefix license:)

  #:replace (rust-pulldown-cmark-0.9))

(define-public rust-anstyle-1
  (package
    (name "rust-anstyle")
    (version "1.0.9")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "anstyle" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0mlc5jmbrgikk700m1sjwgil019rm4mhkq5gzks5y0vcn59dwrc3"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t))
    (home-page "https://github.com/rust-cli/anstyle")
    (synopsis "ANSI text styling")
    (description "This package provides ANSI text styling.")
    (license (list license:expat license:asl2.0))))

(define-public rust-anstyle-parse-0.2
  (package
    (name "rust-anstyle-parse")
    (version "0.2.6")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "anstyle-parse" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1acqayy22fwzsrvr6n0lz6a4zvjjcvgr5sm941m7m0b2fr81cb9v"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-utf8parse" ,rust-utf8parse-0.2))))
    (home-page "https://github.com/rust-cli/anstyle")
    (synopsis "Parse ANSI Style Escapes")
    (description "This package provides Parse ANSI Style Escapes.")
    (license (list license:expat license:asl2.0))))

(define-public rust-anstyle-query-1
  (package
    (name "rust-anstyle-query")
    (version "1.1.2")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "anstyle-query" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "036nm3lkyk43xbps1yql3583fp4hg3b1600is7mcyxs1gzrpm53r"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-windows-sys" ,rust-windows-sys-0.59))))
    (home-page "https://github.com/rust-cli/anstyle.git")
    (synopsis "Look up colored console capabilities")
    (description "This package provides Look up colored console capabilities.")
    (license (list license:expat license:asl2.0))))

(define-public rust-arboard-3
  (package
    (name "rust-arboard")
    (version "3.4.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "arboard" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "12bzkkfgb8dy2hizf8928hs1sai4yhqbrg55a0a8zzz86fah1d4z"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-clipboard-win" ,rust-clipboard-win-5)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-objc2" ,rust-objc2-0.5)
                       ("rust-objc2-app-kit" ,rust-objc2-app-kit-0.2)
                       ("rust-objc2-foundation" ,rust-objc2-foundation-0.2)
                       ("rust-parking-lot" ,rust-parking-lot-0.12)
                       ("rust-windows-sys" ,rust-windows-sys-0.48)
                       ("rust-x11rb" ,rust-x11rb-0.13))))
    (home-page "https://github.com/1Password/arboard")
    (synopsis "Image and text handling for the OS clipboard")
    (description
     "This package provides Image and text handling for the OS clipboard.")
    (license (list license:expat license:asl2.0))))

(define-public rust-assert-fs-1
  (package
    (name "rust-assert-fs")
    (version "1.1.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "assert_fs" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1v52l81h93pwk40c7g7bg3g7276cb2afbkdn26dn5vf823hn5mrc"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-anstyle" ,rust-anstyle-1)
                       ("rust-doc-comment" ,rust-doc-comment-0.3)
                       ("rust-globwalk" ,rust-globwalk-0.9)
                       ("rust-predicates" ,rust-predicates-3)
                       ("rust-predicates-core" ,rust-predicates-core-1)
                       ("rust-predicates-tree" ,rust-predicates-tree-1)
                       ("rust-tempfile" ,rust-tempfile-3))))
    (home-page "https://github.com/assert-rs/assert_fs")
    (synopsis "Filesystem fixtures and assertions for testing")
    (description
     "This package provides Filesystem fixtures and assertions for testing.")
    (license (list license:expat license:asl2.0))))

(define-public rust-bitcode-0.6
  (package
    (name "rust-bitcode")
    (version "0.6.3")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "bitcode" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1167gz68iay84ibi1wjzxjyfc4xz1lnjd94n0azx832n11vcw6zf"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-arrayvec" ,rust-arrayvec-0.7)
                       ("rust-bitcode-derive" ,rust-bitcode-derive-0.6)
                       ("rust-bytemuck" ,rust-bytemuck-1)
                       ("rust-glam" ,rust-glam-0.21)
                       ("rust-serde" ,rust-serde-1))
       #:cargo-development-inputs (("rust-arrayvec" ,rust-arrayvec-0.7)
                                   ("rust-bincode" ,rust-bincode-1)
                                   ("rust-flate2" ,rust-flate2-1)
                                   ("rust-glam" ,rust-glam-0.22)
                                   ("rust-lz4-flex" ,rust-lz4-flex-0.11)
                                   ("rust-paste" ,rust-paste-1)
                                   ("rust-rand" ,rust-rand-0.8)
                                   ("rust-rand-chacha" ,rust-rand-chacha-0.3)
                                   ("rust-serde" ,rust-serde-1)
                                   ("rust-zstd" ,rust-zstd-0.13))))
    (home-page "https://github.com/SoftbearStudios/bitcode")
    (synopsis "bitcode is a bitwise binary serializer")
    (description
     "This package provides bitcode is a bitwise binary serializer.")
    (license (list license:expat license:asl2.0))))

(define-public rust-bitcode-derive-0.6
  (package
    (name "rust-bitcode-derive")
    (version "0.6.3")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "bitcode_derive" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1giy4ndl3lsws8ij42v6dlqfnbf1vvkpmd528p9jq2dg2fd3hfd5"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-proc-macro2" ,rust-proc-macro2-1)
                       ("rust-quote" ,rust-quote-1)
                       ("rust-syn" ,rust-syn-2))))
    (home-page "https://github.com/SoftbearStudios/bitcode/")
    (synopsis "Implementation of #[derive(Encode, Decode)] for bitcode")
    (description
     "This package provides Implementation of #[derive(Encode, Decode)] for bitcode.")
    (license (list license:expat license:asl2.0))))

(define-public rust-cc-1.0.90
  (package
    (name "rust-cc")
    (version "1.0.90")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "cc" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1xg1bqnq50dpf6g1hl90caxgz4afnf74pxa426gh7wxch9561mlc"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-jobserver" ,rust-jobserver-0.1)
                       ("rust-libc" ,rust-libc-0.2))
       #:cargo-development-inputs (("rust-tempfile" ,rust-tempfile-3))))
    (home-page "https://github.com/rust-lang/cc-rs")
    (synopsis
     "build-time dependency for Cargo build scripts to assist in invoking the native
C compiler to compile native C code into a static archive to be linked into Rust
code.")
    (description
     "This package provides a build-time dependency for Cargo build scripts to assist
in invoking the native C compiler to compile native C code into a static archive
to be linked into Rust code.")
    (license (list license:expat license:asl2.0))))

(define-public rust-colorchoice-1
  (package
    (name "rust-colorchoice")
    (version "1.0.3")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "colorchoice" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1439m3r3jy3xqck8aa13q658visn71ki76qa93cy55wkmalwlqsv"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t))
    (home-page "https://github.com/rust-cli/anstyle.git")
    (synopsis "Global override of color control")
    (description "This package provides Global override of color control.")
    (license (list license:expat license:asl2.0))))

(define-public rust-daemonize-0.4
  (package
    (name "rust-daemonize")
    (version "0.4.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "daemonize" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "05cqr2zjxrxyg23snykd03sgqwxn0pvwj2lzh50bclsgwc9lbhkh"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-boxfnonce" ,rust-boxfnonce-0.1)
                       ("rust-libc" ,rust-libc-0.2))
       #:cargo-development-inputs (("rust-tempdir" ,rust-tempdir-0.3))))
    (home-page "https://github.com/knsd/daemonize")
    (synopsis
     "Library to enable your code run as a daemon process on Unix-like systems")
    (description
     "This package provides Library to enable your code run as a daemon process
on Unix-like systems.")
    (license (list license:expat license:asl2.0))))

(define-public rust-fast-image-resize-3
  (package
    (name "rust-fast-image-resize")
    (version "3.0.4")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "fast_image_resize" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "12vyjp14nk49kk25jgbfvl2gyrvp1wzig5jmh9rasd53r3x51m69"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-cfg-if" ,rust-cfg-if-1)
                       ("rust-num-traits" ,rust-num-traits-0.2)
                       ("rust-thiserror" ,rust-thiserror-1))
       #:cargo-development-inputs (("rust-criterion" ,rust-criterion-0.5)
                                   ("rust-image" ,rust-image-0.24)
                                   ("rust-itertools" ,rust-itertools-0.12)
                                   ("rust-libvips" ,rust-libvips-1)
                                   ("rust-nix" ,rust-nix-0.27)
                                   ("rust-png" ,rust-png-0.17)
                                   ("rust-resize" ,rust-resize-0.8)
                                   ("rust-rgb" ,rust-rgb-0.8)
                                   ("rust-serde" ,rust-serde-1)
                                   ("rust-serde-json" ,rust-serde-json-1)
                                   ("rust-tera" ,rust-tera-1)
                                   ("rust-walkdir" ,rust-walkdir-2))))
    (home-page "https://github.com/cykooz/fast_image_resize")
    (synopsis
     "Library for fast image resizing with using of SIMD instructions")
    (description
     "This package provides Library for fast image resizing with using of SIMD instructions.")
    (license (list license:expat license:asl2.0))))

(define-public rust-fs4-0.7
  (package
    (name "rust-fs4")
    (version "0.7.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "fs4" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1xr8gph0lvwwmh60nir7m6f5sl3w2fh1hbgb6lrlwb48265dzy99"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-rustix" ,rust-rustix-0.38)
                       ("rust-windows-sys" ,rust-windows-sys-0.48))))
    (home-page "https://github.com/al8n/fs4-rs")
    (synopsis "No libc, pure Rust cross-platform file locks. Original fs2,
now supports async and replace libc by rustix.")
    (description "No libc, pure Rust cross-platform file locks.  Original fs2,
now supports async and replace libc by rustix.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-libredox-0.0.1
  (package
    (name "rust-libredox")
    (version "0.0.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "libredox" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1s2fh4ikpp9xl0lsl01pi0n8pw1q9s3ld452vd8qh1v63v537j45"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-2)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-redox-syscall" ,rust-redox-syscall-0.4))))
    (home-page "https://gitlab.redox-os.org/redox-os/libredox.git")
    (synopsis "Redox stable ABI.")
    (description "This package provides Redox's stable ABI.")
    (license license:expat)))

(define-public rust-libvips-1
  (package
    (name "rust-libvips")
    (version "1.4.3")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "libvips" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1qmcd4wx4mn0yhm8s7q3hrk59x18cia4f3fpdd1ww8rq5fh78rcw"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-num-derive" ,rust-num-derive-0.3)
                       ("rust-num-traits" ,rust-num-traits-0.2))))
    (home-page "https://github.com/olxgroup-oss/libvips-rust-bindings")
    (synopsis "Safe bindings for libvips")
    (description "This package provides Safe bindings for libvips.")
    (license license:expat)))

(define-public rust-lz4-flex-0.11
  (package
    (name "rust-lz4-flex")
    (version "0.11.3")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "lz4_flex" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1xg3h3y0ghnq3widdssd36s02pvy29c0afbwgq6mh3ibmri12xkm"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-twox-hash" ,rust-twox-hash-1))))
    (home-page "https://github.com/pseitz/lz4_flex")
    (synopsis "Fastest LZ4 implementation in Rust, no unsafe by default")
    (description
     "This package provides Fastest LZ4 implementation in Rust, no unsafe by default.")
    (license license:expat)))

(define-public rust-memoffset-0.9
  (package
    (name "rust-memoffset")
    (version "0.9.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "memoffset" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "12i17wh9a9plx869g7j4whf62xw68k5zd4k0k5nh6ys5mszid028"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-autocfg" ,rust-autocfg-1))))
    (home-page "https://github.com/Gilnaa/memoffset")
    (synopsis "offset_of functionality for Rust structs")
    (description
     "This package provides offset_of functionality for Rust structs.")
    (license license:expat)))

(define-public rust-oorandom-11
  (package
    (name "rust-oorandom")
    (version "11.1.4")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "oorandom" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1sg4j19r5302a6jpn0kgfkbjnslrqr3ynxv8x2h2ddaaw7kvn45l"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t))
    (home-page "https://hg.sr.ht/~icefox/oorandom")
    (synopsis "tiny, robust PRNG implementation.")
    (description "This package provides a tiny, robust PRNG implementation.")
    (license license:expat)))

(define-public rust-procfs-0.14
  (package
    (name "rust-procfs")
    (version "0.14.2")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "procfs" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0sdv4r3gikcz12qzb4020rlcq7vn8kh72vgwmvk7fgw7n2n8vpmi"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-1)
                       ("rust-byteorder" ,rust-byteorder-1)
                       ("rust-hex" ,rust-hex-0.4)
                       ("rust-lazy-static" ,rust-lazy-static-1)
                       ("rust-rustix" ,rust-rustix-0.36))))
    (home-page "https://github.com/eminence/procfs")
    (synopsis "Interface to the linux procfs pseudo-filesystem")
    (description
     "This package provides Interface to the linux procfs pseudo-filesystem.")
    (license (list license:expat license:asl2.0))))

(define-public rust-procfs-0.9
  (package
    (name "rust-procfs")
    (version "0.9.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "procfs" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1kfmlpx902czawrn54zbs0918k0bxi22lv931zds4l44q7h0k25b"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-backtrace" ,rust-backtrace-0.3)
                       ("rust-bitflags" ,rust-bitflags-1)
                       ("rust-byteorder" ,rust-byteorder-1)
                       ("rust-chrono" ,rust-chrono-0.4)
                       ("rust-flate2" ,rust-flate2-1)
                       ("rust-hex" ,rust-hex-0.4)
                       ("rust-lazy-static" ,rust-lazy-static-1)
                       ("rust-libc" ,rust-libc-0.2))
       #:cargo-development-inputs (("rust-procinfo" ,rust-procinfo-0.4))))
    (home-page "https://github.com/eminence/procfs")
    (synopsis "Interface to the linux procfs pseudo-filesystem")
    (description
     "This package provides Interface to the linux procfs pseudo-filesystem.")
    (license (list license:expat license:asl2.0))))

(define-public rust-pulldown-cmark-0.9
  (package
    (name "rust-pulldown-cmark")
    (version "0.9.6")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "pulldown-cmark" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0av876a31qvqhy7gzdg134zn4s10smlyi744mz9vrllkf906n82p"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-2)
                       ("rust-getopts" ,rust-getopts-0.2)
                       ("rust-memchr" ,rust-memchr-2)
                       ("rust-unicase" ,rust-unicase-2))))
    (home-page "https://github.com/raphlinus/pulldown-cmark")
    (synopsis "pull parser for CommonMark")
    (description "This package provides a pull parser for @code{CommonMark}.")
    (license license:expat)))

(define-public rust-ratatui-macros-0.4
  (package
    (name "rust-ratatui-macros")
    (version "0.4.2")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "ratatui-macros" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0zbrw0ybknddbmd23zb3nfhgigndac4jgr6fzsk13q9iwh419ipq"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-ratatui" ,rust-ratatui-0.27))))
    (home-page "https://github.com/ratatui-org/ratatui-macros")
    (synopsis "Macros for Ratatui")
    (description "This package provides Macros for Ratatui.")
    (license license:expat)))

(define-public rust-redox-syscall-0.5
  (package
    (name "rust-redox-syscall")
    (version "0.5.3")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "redox_syscall" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1916m7abg9649gkif055pn5nsvqjhp70isy0v7gx1zgi01p8m41a"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-2))))
    (home-page "https://gitlab.redox-os.org/redox-os/syscall")
    (synopsis "Rust library to access raw Redox system calls")
    (description
     "This package provides a Rust library to access raw Redox system calls.")
    (license license:expat)))

(define-public rust-sd-notify-0.4
  (package
    (name "rust-sd-notify")
    (version "0.4.3")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "sd-notify" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0la4d28ym4rarm8bax07yh9f5r0jb2iqxwmjz00ffgirgxghrqhv"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-sendfd" ,rust-sendfd-0.4))))
    (home-page "https://github.com/lnicola/sd-notify")
    (synopsis "Lightweight crate for systemd service state notifications")
    (description
     "This package provides Lightweight crate for systemd service state notifications.")
    (license (list license:expat license:asl2.0))))

(define-public rust-sendfd-0.4
  (package
    (name "rust-sendfd")
    (version "0.4.3")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "sendfd" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "160pf2dp5r8smjc6ssk6jf9k93vc280wk8i362xi6zi6zjw72jv0"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-libc" ,rust-libc-0.2)
                       ("rust-tokio" ,rust-tokio-1))))
    (home-page "https://github.com/standard-ai/sendfd")
    (synopsis "Send file descriptors along with data over UNIX domain sockets")
    (description
     "This package provides Send file descriptors along with data over UNIX domain sockets.")
    (license (list license:asl2.0 license:bsd-3))))

(define-public rust-serde-yaml-0.9
  (package
    (name "rust-serde-yaml")
    (version "0.9.34+deprecated")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "serde_yaml" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0isba1fjyg3l6rxk156k600ilzr8fp7crv82rhal0rxz5qd1m2va"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-indexmap" ,rust-indexmap-2)
                       ("rust-itoa" ,rust-itoa-1)
                       ("rust-ryu" ,rust-ryu-1)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-unsafe-libyaml" ,rust-unsafe-libyaml-0.2))))
    (home-page "https://github.com/dtolnay/serde-yaml")
    (synopsis "YAML data format for Serde")
    (description "This package provides YAML data format for Serde.")
    (license (list license:expat license:asl2.0))))

(define-public rust-signal-hook-registry-1
  (package
    (name "rust-signal-hook-registry")
    (version "1.4.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "signal-hook-registry" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "18crkkw5k82bvcx088xlf5g4n3772m24qhzgfan80nda7d3rn8nq"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-libc" ,rust-libc-0.2))))
    (home-page "https://github.com/vorner/signal-hook")
    (synopsis "Backend crate for signal-hook")
    (description "This package provides Backend crate for signal-hook.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-simple-logger-5
  (package
    (name "rust-simple-logger")
    (version "5.0.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "simple_logger" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1swpfkym2f3pajcblskk7h82gmcljyfzs3xa0hvmarw7w2jxzig8"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-colored" ,rust-colored-2)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-time" ,rust-time-0.3)
                       ("rust-windows-sys" ,rust-windows-sys-0.48))))
    (home-page "https://github.com/borntyping/rust-simple_logger")
    (synopsis "logger that prints all messages with a readable output format")
    (description
     "This package provides a logger that prints all messages with a readable output
format.")
    (license license:expat)))

(define-public rust-slog-term-2
  (package
    (name "rust-slog-term")
    (version "2.9.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "slog-term" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1s0h8qhqnvy5a7m7gmnca2a2d5m5a4sz1hc26xfgxawqp7825q5n"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-is-terminal" ,rust-is-terminal-0.4)
                       ("rust-slog" ,rust-slog-2)
                       ("rust-term" ,rust-term-0.7)
                       ("rust-thread-local" ,rust-thread-local-1)
                       ("rust-time" ,rust-time-0.3))))
    (home-page "https://github.com/slog-rs/slog")
    (synopsis "Unix terminal drain and formatter for slog-rs")
    (description
     "This package provides Unix terminal drain and formatter for slog-rs.")
    (license (list license:mpl2.0 license:expat license:asl2.0))))

(define-public rust-snapbox-0.6
  (package
    (name "rust-snapbox")
    (version "0.6.18")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "snapbox" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1g2ldaihkdhin8cabjcgms53952ssxmqsa04ch81p6waia0k993v"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-anstream" ,rust-anstream-0.6)
                       ("rust-anstyle" ,rust-anstyle-1)
                       ("rust-anstyle-svg" ,rust-anstyle-svg-0.1)
                       ("rust-backtrace" ,rust-backtrace-0.3)
                       ("rust-content-inspector" ,rust-content-inspector-0.2)
                       ("rust-document-features" ,rust-document-features-0.2)
                       ("rust-dunce" ,rust-dunce-1)
                       ("rust-escargot" ,rust-escargot-0.5)
                       ("rust-filetime" ,rust-filetime-0.2)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-normalize-line-endings" ,rust-normalize-line-endings-0.3)
                       ("rust-os-pipe" ,rust-os-pipe-1)
                       ("rust-regex" ,rust-regex-1)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-serde-json" ,rust-serde-json-1)
                       ("rust-similar" ,rust-similar-2)
                       ("rust-snapbox-macros" ,rust-snapbox-macros-0.3)
                       ("rust-tempfile" ,rust-tempfile-3)
                       ("rust-wait-timeout" ,rust-wait-timeout-0.2)
                       ("rust-walkdir" ,rust-walkdir-2)
                       ("rust-windows-sys" ,rust-windows-sys-0.59))))
    (home-page "https://github.com/assert-rs/trycmd/tree/main/crates/snapbox")
    (synopsis "Snapshot testing toolbox")
    (description "This package provides Snapshot testing toolbox.")
    (license (list license:expat license:asl2.0))))

(define-public rust-spin-sleep-1
  (package
    (name "rust-spin-sleep")
    (version "1.3.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "spin_sleep" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "12ki8v0g254v4a7mqziqhwmvyzv54zld1x747da47i0xihfb75j1"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-windows-sys" ,rust-windows-sys-0.59))
       #:cargo-development-inputs (("rust-approx" ,rust-approx-0.5))))
    (home-page "https://github.com/alexheretic/spin-sleep")
    (synopsis
     "Accurate sleeping. Only use native sleep as far as it can be trusted,
then spin")
    (description
     "This package provides Accurate sleeping.  Only use native sleep as far as
it can be trusted, then spin.")
    (license license:asl2.0)))

(define-public rust-thread-local-1
  (package
    (name "rust-thread-local")
    (version "1.1.8")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "thread_local" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "173i5lyjh011gsimk21np9jn8al18rxsrkjli20a7b8ks2xgk7lb"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-cfg-if" ,rust-cfg-if-1)
                       ("rust-once-cell" ,rust-once-cell-1))))
    (home-page "https://github.com/Amanieu/thread_local-rs")
    (synopsis "Per-object thread-local storage")
    (description "This package provides Per-object thread-local storage.")
    (license (list license:expat license:asl2.0))))

(define-public rust-tinyvec-1
  (package
    (name "rust-tinyvec")
    (version "1.8.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "tinyvec" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0f5rf6a2wzyv6w4jmfga9iw7rp9fp5gf4d604xgjsf3d9wgqhpj4"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-tinyvec-macros" ,rust-tinyvec-macros-0.1))))
    (home-page "https://github.com/Lokathor/tinyvec")
    (synopsis "`tinyvec` provides 100% safe vec-like data structures")
    (description
     "This package provides `tinyvec` provides 100% safe vec-like data structures.")
    (license (list license:zlib license:asl2.0 license:expat))))

(define-public rust-trackable-1
  (package
    (name "rust-trackable")
    (version "1.3.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "trackable" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1bk96cvr587cdhz8xl7jhkqn7vksyg41grbpx77gi7mrmcad2nxi"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-trackable-derive" ,rust-trackable-derive-1))))
    (home-page "https://github.com/sile/trackable")
    (synopsis "Track objects manually as an alternative to backtracing")
    (description "This library provides a way to track objects manually as an
alternative to mechanisms like backtracing.")
    (license license:expat)))

(define-public rust-tree-sitter-bash-0.20
  (package
    (name "rust-tree-sitter-bash")
    (version "0.20.5")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "tree-sitter-bash" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "01jr5dadyma9jf08ykc5dn6yqprmjhxpvpqqzllwxckyqcr21njp"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-cc" ,rust-cc-1)
                       ("rust-tree-sitter" ,rust-tree-sitter-0.20))))
    (home-page "https://github.com/tree-sitter/tree-sitter-bash")
    (synopsis "Bash grammar for tree-sitter")
    (description "This package provides Bash grammar for tree-sitter.")
    (license license:expat)))

(define-public rust-tree-sitter-c-0.20
  (package
    (name "rust-tree-sitter-c")
    (version "0.20.8")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "tree-sitter-c" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0wkbrjwi68pmqpv6n9791yh9ybhw75narbgjz20qbh2qhqymzgab"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-cc" ,rust-cc-1)
                       ("rust-tree-sitter" ,rust-tree-sitter-0.20))))
    (home-page "https://github.com/tree-sitter/tree-sitter-c")
    (synopsis "C grammar for tree-sitter")
    (description "This package provides C grammar for tree-sitter.")
    (license license:expat)))

(define-public rust-tree-sitter-c-sharp-0.20
  (package
    (name "rust-tree-sitter-c-sharp")
    (version "0.20.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "tree-sitter-c-sharp" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0cdf37z6dfiff83p3p8dvsv19g1dysakllqhkvx28jgk1333vaxr"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-cc" ,rust-cc-1)
                       ("rust-tree-sitter" ,rust-tree-sitter-0.20))))
    (home-page "https://github.com/tree-sitter/tree-sitter-c-sharp")
    (synopsis "C# grammar for tree-sitter")
    (description "This package provides C# grammar for tree-sitter.")
    (license license:expat)))

(define-public rust-tree-sitter-cpp-0.20
  (package
    (name "rust-tree-sitter-cpp")
    (version "0.20.5")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "tree-sitter-cpp" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0x5vzrykwmr6vc3115x15s6wqjh9q5n6m5lmk3xrl1biv9d4mc26"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-cc" ,rust-cc-1)
                       ("rust-tree-sitter" ,rust-tree-sitter-0.20))))
    (home-page "https://github.com/tree-sitter/tree-sitter-cpp")
    (synopsis "C++ grammar for tree-sitter")
    (description "This package provides C++ grammar for tree-sitter.")
    (license license:expat)))

(define-public rust-tree-sitter-elixir-0.1
  (package
    (name "rust-tree-sitter-elixir")
    (version "0.1.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "tree-sitter-elixir" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0ns1664w51gpbv86bxfz57m3lkhmzlr1y5z55ai2rwfrwvrv3h0v"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-cc" ,rust-cc-1)
                       ("rust-tree-sitter" ,rust-tree-sitter-0.20))))
    (home-page "https://github.com/elixir-lang/tree-sitter-elixir")
    (synopsis "Elixir grammar for the tree-sitter parsing library")
    (description
     "This package provides Elixir grammar for the tree-sitter parsing library.")
    (license license:asl2.0)))

(define-public rust-tree-sitter-go-0.20
  (package
    (name "rust-tree-sitter-go")
    (version "0.20.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "tree-sitter-go" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "052irhc00xfkl4v3is3ddmgk5b0dblpz39zxy8d9c6s434gx3mhs"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-cc" ,rust-cc-1)
                       ("rust-tree-sitter" ,rust-tree-sitter-0.20))))
    (home-page "https://github.com/tree-sitter/tree-sitter-go")
    (synopsis "Go grammar for tree-sitter")
    (description "This package provides Go grammar for tree-sitter.")
    (license license:expat)))

(define-public rust-tree-sitter-haskell-0.15
  (package
    (name "rust-tree-sitter-haskell")
    (version "0.15.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "tree-sitter-haskell" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1wml8ma57rd33f1j5kgqqa5fs0sm7ywg8c88ph37f4ncss35nqxc"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-cc" ,rust-cc-1)
                       ("rust-tree-sitter" ,rust-tree-sitter-0.20))))
    (home-page "https://github.com/tree-sitter/tree-sitter-haskell")
    (synopsis "Haskell grammar for tree-sitter")
    (description "This package provides Haskell grammar for tree-sitter.")
    (license license:expat)))

(define-public rust-tree-sitter-highlight-0.20
  (package
    (name "rust-tree-sitter-highlight")
    (version "0.20.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "tree-sitter-highlight" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1g3811rccl8lw2hkkcy4qvfni61zmfyy5i4z7n1hnyjs9ic448q4"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-regex" ,rust-regex-1)
                       ("rust-thiserror" ,rust-thiserror-1)
                       ("rust-tree-sitter" ,rust-tree-sitter-0.20))))
    (home-page "https://tree-sitter.github.io/tree-sitter")
    (synopsis "Library for performing syntax highlighting with Tree-sitter")
    (description
     "This package provides Library for performing syntax highlighting with Tree-sitter.")
    (license license:expat)))

(define-public rust-tree-sitter-html-0.20
  (package
    (name "rust-tree-sitter-html")
    (version "0.20.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "tree-sitter-html" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "07gsnzb0ydbi73cjsa36x1a35qhwyqsbiavzsr5kr122pnv24y01"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-cc" ,rust-cc-1)
                       ("rust-tree-sitter" ,rust-tree-sitter-0.20))))
    (home-page "https://github.com/tree-sitter/tree-sitter-html")
    (synopsis "HTML grammar for tree-sitter")
    (description "This package provides HTML grammar for tree-sitter.")
    (license license:expat)))

(define-public rust-tree-sitter-java-0.20
  (package
    (name "rust-tree-sitter-java")
    (version "0.20.2")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "tree-sitter-java" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0lr68vpj1ryp7zv18hjgjql3n3idp0mpsifph487dgsspyb5dp1a"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-cc" ,rust-cc-1)
                       ("rust-tree-sitter" ,rust-tree-sitter-0.20))))
    (home-page "https://github.com/tree-sitter/tree-sitter-java")
    (synopsis "Java grammar for tree-sitter")
    (description "This package provides Java grammar for tree-sitter.")
    (license license:expat)))

(define-public rust-tree-sitter-javascript-0.20
  (package
    (name "rust-tree-sitter-javascript")
    (version "0.20.4")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "tree-sitter-javascript" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0b1wywi2kr7nhk20rnm7lz1xz1ij70fgg7rjyw3chqlbm4pc05fh"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-cc" ,rust-cc-1.0.90)
                       ("rust-tree-sitter" ,rust-tree-sitter-0.20))))
    (home-page "https://github.com/tree-sitter/tree-sitter-javascript")
    (synopsis "JavaScript grammar for tree-sitter")
    (description
     "This package provides @code{JavaScript} grammar for tree-sitter.")
    (license license:expat)))

(define-public rust-tree-sitter-json-0.20
  (package
    (name "rust-tree-sitter-json")
    (version "0.20.2")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "tree-sitter-json" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0kgdhpj1ci9lgwcwygx52wdglscdq0g3hl3ks745rdbrqslki6js"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-cc" ,rust-cc-1)
                       ("rust-tree-sitter" ,rust-tree-sitter-0.20))))
    (home-page "https://github.com/tree-sitter/tree-sitter-json")
    (synopsis "JSON grammar for tree-sitter")
    (description "This package provides JSON grammar for tree-sitter.")
    (license license:expat)))

(define-public rust-tree-sitter-ocaml-0.20
  (package
    (name "rust-tree-sitter-ocaml")
    (version "0.20.4")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "tree-sitter-ocaml" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "12i0mkrj83lgn3rp9pi9hyws407fsd5qzggzxkh8mksqqsmn64gx"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-cc" ,rust-cc-1)
                       ("rust-tree-sitter" ,rust-tree-sitter-0.20))))
    (home-page "https://github.com/tree-sitter/tree-sitter-ocaml")
    (synopsis "OCaml grammar for tree-sitter")
    (description "This package provides OCaml grammar for tree-sitter.")
    (license license:expat)))

(define-public rust-tree-sitter-php-0.20
  (package
    (name "rust-tree-sitter-php")
    (version "0.20.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "tree-sitter-php" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1plp0wrj2c6j8pksfi46fqb7nwvz9q02h1swgrqg1lbvlnm8kdhq"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-cc" ,rust-cc-1)
                       ("rust-tree-sitter" ,rust-tree-sitter-0.20))))
    (home-page "https://github.com/tree-sitter/tree-sitter-php")
    (synopsis "PHP grammar for tree-sitter")
    (description "This package provides PHP grammar for tree-sitter.")
    (license license:expat)))

(define-public rust-tree-sitter-python-0.20
  (package
    (name "rust-tree-sitter-python")
    (version "0.20.4")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "tree-sitter-python" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "19cajx86brj6vgdjvxng9l5kvr2q63ym2ps4nffkj3dx3wdkpjg6"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-cc" ,rust-cc-1)
                       ("rust-tree-sitter" ,rust-tree-sitter-0.20))))
    (home-page "https://github.com/tree-sitter/tree-sitter-python")
    (synopsis "Python grammar for tree-sitter")
    (description "This package provides Python grammar for tree-sitter.")
    (license license:expat)))

(define-public rust-tree-sitter-ruby-0.20
  (package
    (name "rust-tree-sitter-ruby")
    (version "0.20.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "tree-sitter-ruby" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1zi74hxv8196l1xjl6b80fwqrvdc3yq5yk02bx4gi7a6hgrhxma4"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-cc" ,rust-cc-1)
                       ("rust-tree-sitter" ,rust-tree-sitter-0.20))))
    (home-page "https://github.com/tree-sitter/tree-sitter-ruby")
    (synopsis "Ruby grammar for tree-sitter")
    (description "This package provides Ruby grammar for tree-sitter.")
    (license license:expat)))

(define-public rust-tree-sitter-rust-0.20
  (package
    (name "rust-tree-sitter-rust")
    (version "0.20.4")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "tree-sitter-rust" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "153mz8k8jh1l87z6nlb8nir1szmlij0hwp6fc0vx7dmjn04j70xh"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-cc" ,rust-cc-1.0.90)
                       ("rust-tree-sitter" ,rust-tree-sitter-0.20))))
    (home-page "https://github.com/tree-sitter/tree-sitter-rust")
    (synopsis "Rust grammar for tree-sitter")
    (description "This package provides Rust grammar for tree-sitter.")
    (license license:expat)))

(define-public rust-tree-sitter-scala-0.20
  (package
    (name "rust-tree-sitter-scala")
    (version "0.20.3")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "tree-sitter-scala" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0l53vld3qs6gp5d29ynxmd2ybwr4p5c2dx9zmk5vb8w8i9ig9z24"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-cc" ,rust-cc-1)
                       ("rust-tree-sitter" ,rust-tree-sitter-0.20))))
    (home-page "https://github.com/tree-sitter/tree-sitter-scala")
    (synopsis "Scala grammar for tree-sitter")
    (description "This package provides Scala grammar for tree-sitter.")
    (license license:expat)))

(define-public rust-tree-sitter-toml-0.20
  (package
    (name "rust-tree-sitter-toml")
    (version "0.20.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "tree-sitter-toml" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1s2kkcrw6kinb9x64nxdzwgahzs0i0kcqiq2g0h3vclqi9bpylfa"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-cc" ,rust-cc-1)
                       ("rust-tree-sitter" ,rust-tree-sitter-0.20))))
    (home-page "https://github.com/Mathspy/tree-sitter-toml")
    (synopsis "TOML grammar for the tree-sitter parsing library")
    (description
     "This package provides TOML grammar for the tree-sitter parsing library.")
    (license license:expat)))

(define-public rust-tree-sitter-typescript-0.20
  (package
    (name "rust-tree-sitter-typescript")
    (version "0.20.5")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "tree-sitter-typescript" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0w4nc7yjg2k64jrx1y782wvvd7dci1lbawbs17plhsi74hn1vg68"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-cc" ,rust-cc-1)
                       ("rust-tree-sitter" ,rust-tree-sitter-0.20))))
    (home-page "https://github.com/tree-sitter/tree-sitter-typescript")
    (synopsis "TypeScript and TSX grammars for tree-sitter")
    (description
     "This package provides @code{TypeScript} and TSX grammars for tree-sitter.")
    (license license:expat)))

(define-public rust-tui-prompts-0.3
  (package
    (name "rust-tui-prompts")
    (version "0.3.20")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "tui-prompts" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "044hk3bbkaa84fmyc9kkkvjv1fgjr6wcwwr9r51afbqm0i6khlmm"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-itertools" ,rust-itertools-0.13)
                       ("rust-ratatui" ,rust-ratatui-0.27)
                       ("rust-ratatui-macros" ,rust-ratatui-macros-0.4)
                       ("rust-rstest" ,rust-rstest-0.21))))
    (home-page "https://github.com/joshka/tui-widgets")
    (synopsis "library for building interactive prompts for ratatui.")
    (description
     "This package provides a library for building interactive prompts for ratatui.")
    (license (list license:expat license:asl2.0))))

(define-public rust-utf8parse-0.2
  (package
    (name "rust-utf8parse")
    (version "0.2.2")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "utf8parse" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "088807qwjq46azicqwbhlmzwrbkz7l4hpw43sdkdyyk524vdxaq6"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t))
    (home-page "https://github.com/alacritty/vte")
    (synopsis "Table-driven UTF-8 parser")
    (description "This package provides Table-driven UTF-8 parser.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-windows-0.43
  (package
    (name "rust-windows")
    (version "0.43.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "windows" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0i32alvc4n4l7abmv1fsvnd1lzw17f1cpr16kgx0sqz5wg82wrh4"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-windows-implement" ,rust-windows-implement-0.43)
                       ("rust-windows-interface" ,rust-windows-interface-0.43)
                       ("rust-windows-aarch64-gnullvm" ,rust-windows-aarch64-gnullvm-0.42)
                       ("rust-windows-aarch64-msvc" ,rust-windows-aarch64-msvc-0.42)
                       ("rust-windows-i686-gnu" ,rust-windows-i686-gnu-0.42)
                       ("rust-windows-i686-msvc" ,rust-windows-i686-msvc-0.42)
                       ("rust-windows-x86-64-gnu" ,rust-windows-x86-64-gnu-0.42)
                       ("rust-windows-x86-64-gnullvm" ,rust-windows-x86-64-gnullvm-0.42)
                       ("rust-windows-x86-64-msvc" ,rust-windows-x86-64-msvc-0.42))))
    (home-page "https://github.com/microsoft/windows-rs")
    (synopsis "Rust for Windows")
    (description "This package provides Rust for Windows.")
    (license (list license:expat license:asl2.0))))

(define-public rust-windows-core-0.51
  (package
    (name "rust-windows-core")
    (version "0.51.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "windows-core" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0r1f57hsshsghjyc7ypp2s0i78f7b1vr93w68sdb8baxyf2czy7i"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-windows-targets" ,rust-windows-targets-0.48))))
    (home-page "https://github.com/microsoft/windows-rs")
    (synopsis "Rust for Windows.")
    (description "Rust for Windows.")
    (license (list license:asl2.0 license:expat))))
