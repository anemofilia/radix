(define-module (radix packages kak-xyz)
  #:use-module (gnu packages crates-apple)
  #:use-module (gnu packages crates-io)
  #:use-module (gnu packages crates-windows)
  #:use-module (guix build-system cargo)
  #:use-module (guix build-system gnu)
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix build-system copy)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (radix packages crates-io))

(define-public kak-phantom-selection
  (let ((commit "ffbed4ac8a9b2f0277213ca479a62ccc6620e33d")
        (revision "0"))
    (package
      (name "kak-phantom-selection")
      (version (git-version "0.0.0" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/occivink/kakoune-phantom-selection")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32 "1q2q9a4hw8fm4q8brq16gmwazj9cplbspblv1lcmfrbf5xlfhwia"))))
      (build-system copy-build-system)
      (arguments
       (list #:install-plan
             #~'(("phantom-selection.kak"
                  "share/kak/autoload/plugins/kakoune-phantom-selection/"))))
      (home-page "https://github.com/occivink/kakoune-phantom-selection")
      (synopsis "Work on multiple selections, one at a time.")
      (description "This package provides kakoune plugin to work on multiple
selection one by one. Just a thin wrapper around marks to solve a common
use-case.")
      (license license:unlicense))))

(define-public kak-surround
  (let ((commit "6e45966444603529b06da5a539bf7f427475bd1f")
        (revision "0"))
    (package
      (name "kak-surround")
      (version (git-version "0.0.0" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/maximbaz/surround.kak")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32 "1i7khyqh55lyqh0a6q1h0jqq74s4l5hzp64v559w8syvmh0xchhk"))))
      (build-system copy-build-system)
      (arguments
       (list #:install-plan
             #~'(("rc/surround.kak"
                  "share/kak/autoload/plugins/kakoune-surround/"))))
      (home-page "https://github.com/maximbaz/surround.kak")
      (synopsis "A plugin to surround selections in kakoune.")
      (description "This package provides a kakoune plugin to surround
selections.")
      (license license:unlicense))))

(define-public kak-state-save
  (let ((commit "e8b6c269ac91eab45b0fe0d2b1f5dd49515ecd00")
        (revision "0"))
    (package
      (name "kak-state-save")
      (version (git-version "0.0.0" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://gitlab.com/Screwtapello/kakoune-state-save")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32 "11g2b8aac032q1mns99vrx0rl9rwlinfkim6rkc910y3sk8yfj8j"))))
      (build-system copy-build-system)
      (arguments
       (list #:install-plan
             #~'(("state-save.kak"
                  "share/kak/autoload/plugins/kakoune-state-save/"))))
      (home-page "https://gitlab.com/Screwtapello/kakoune-state-save")
      (synopsis "A plugin to help Kakoune save and restore state between
sessions.")
      (description "This package provides a plugin to help Kakoune save and
restore state between sessions.")
      (license license:expat))))

(define-public kak-buffers
  (let ((commit "6b2081f5b7d58c72de319a5cba7bf628b6802881")
        (revision "0"))
    (package
      (name "kak-buffers")
      (version (git-version "0.0.0" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/Delapouite/kakoune-buffers")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32 "0pbrgydifw2a8yf3ringyqq91fccfv4lm4v8sk5349hbcz6apr4c"))))
      (build-system copy-build-system)
      (arguments
       (list #:install-plan
             #~'(("buffers.kak"
                  "share/kak/autoload/plugins/kakoune-buffers/"))))
      (home-page "https://github.com/Delapouite/kakoune-buffers")
      (synopsis "A kakoune plugin to ease navigation between opened buffers.")
      (description "This package provides a plugin to ease the navigation
between opened buffers.")
      (license license:expat))))

(define-public kak-board
  (let ((commit "5759dcc5af593ff88a7faecc41a8f549ec440771")
        (revision "0"))
    (package
      (name "kak-board")
      (version (git-version "0.0.0" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/lePerdu/kakboard")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32 "0g8q0rkdnzsfvlibjd1zfcapngfli5aa3qrgmkgdi24n9ad8wzvh"))))
      (build-system copy-build-system)
      (arguments
       (list #:install-plan
             #~'(("kakboard.kak"
                  "share/kak/autoload/plugins/kakboard/"))))
      (home-page "https://github.com/lePerdu/kakboard")
      (synopsis "A kakoune plugin to allow clipboard integration for Kakoune.")
      (description "This package provides a plugin to allow clipboard integration
for kakoune.")
      (license license:expat))))

(define-public kak-rainbow
  (let ((commit "63097fbf478217f8b5aa4f85eaa26c95d7c5243e")
        (revision "0"))
    (package
      (name "kak-rainbow")
      (version (git-version "0.0.0" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/JJK96/kakoune-rainbow")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32 "1hx79mxrajnkxk2f1q38zydgjhf5fb3kwmwvrmjv861rqqpbm49f"))))
      (build-system copy-build-system)
      (arguments
       (list #:install-plan
             #~'(("rainbow.kak"
                  "share/kak/autoload/plugins/kakoune-rainbow/"))))
      (home-page "https://github.com/JJK96/kakoune-rainbow")
      (synopsis "A kakoune plugin to highlight matching parenthesis.")
      (description "This package provides a plugin to highlight matching
parenthesis in kakoune.")
      (license license:expat))))

(define-public kak-auto-pairs
  (let ((commit "d4b33b783ea42a536c848296b5b6d434b4d1470f")
        (revision "0"))
    (package
      (name "kak-auto-pairs")
      (version (git-version "0.0.0" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/alexherbo2/auto-pairs.kak")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32 "017x9g89q0w60y561xhigc0f14ryp4phh1hdna847ca5lvmbxksp"))))
      (build-system copy-build-system)
      (arguments
       (list #:install-plan
             #~'(("rc/auto-pairs.kak"
                  "share/kak/autoload/plugins/auto-pairs/"))))
      (home-page "https://github.com/alexherbo2/auto-pairs.kak")
      (synopsis "A kakoune plugin to enable auto pairing of characters.")
      (description "This package provides a kakoune plugin to enable auto
pairing of characters..")
      (license license:unlicense))))

(define-public kak-rainbower
  (let ((commit "4dcb45b506efc64eed0dbd7e97b8aa703baf1278")
        (revision "1"))
    (package
      (name "kak-rainbower")
      (version (git-version "0.0.0" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/crizan/kak-rainbower")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32 "1s3jwxga8kdzr8zdqya6ysvc79nblr6w7znvnh8l3gq5n34m3f6p"))))
      (build-system gnu-build-system)
      (arguments
       (list #:tests? #f                  ;no tests
             #:phases
             #~(modify-phases %standard-phases
                 (delete 'configure)      ;no configure script
                 (replace 'build
                   (lambda _
                     (chdir "rc")
                     (invoke "g++" "rainbower.cpp" "-O2" "-o" "rainbower")))
                 (replace 'install        ;no install phase
                   (lambda _
                     (let ((dir (string-append #$output
                                               "/share/kak/autoload/plugins/"
                                               #$name)))
                       (install-file "rainbow.kak" dir)
                       (install-file "rainbower" dir)))))))
      (home-page "https://github.com/crizan/kak-rainbower")
      (synopsis "A kakoune plugin to highlight matching parenthesis.")
      (description "This package provides a plugin to highlight matching
parenthesis in kakoune.")
      (license license:expat))))

(define-public kak-tree-sitter
  (let ((commit "4a717751ffe599d1d46f70f8a9ad0bd978ef324a")
        (revision "0"))
    (package
      (name "kak-tree-sitter")
      (version (git-version "1.1.2" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
                (url "https://git.sr.ht/~hadronized/kak-tree-sitter")
                (commit commit)))
         (file-name (string-append name "-" version ".tar.gz"))
         (sha256
          (base32 "14qqlxg4qi59njayc6xmgw62s3mnrba9ndz05cprfyxdwvnnqf3q"))))
      (build-system cargo-build-system)
      (arguments
       `(#:install-source? #f
         #:imported-modules (,@%cargo-build-system-modules
                             (guix build cargo-build-system))
         #:modules ((guix build utils)
                    (guix build cargo-build-system)
                    (ice-9 match))
         #:cargo-inputs (("rust-chrono" ,rust-chrono-0.4)
                         ("rust-clap" ,rust-clap-4)
                         ("rust-colored" ,rust-colored-2)
                         ("rust-ctrlc" ,rust-ctrlc-3)
                         ("rust-daemonize" ,rust-daemonize-0.5)
                         ("rust-dirs" ,rust-dirs-5)
                         ("rust-itertools" ,rust-itertools-0.12)
                         ("rust-libc" ,rust-libc-0.2)
                         ("rust-libloading" ,rust-libloading-0.8)
                         ("rust-log" ,rust-log-0.4)
                         ("rust-mio" ,rust-mio-0.8)
                         ("rust-serde" ,rust-serde-1)
                         ("rust-serde-json" ,rust-serde-json-1)
                         ("rust-simple-logger" ,rust-simple-logger-5)
                         ("rust-thiserror" ,rust-thiserror-1)
                         ("rust-toml" ,rust-toml-0.8)
                         ("rust-tree-sitter" ,rust-tree-sitter-0.20)
                         ("rust-tree-sitter-highlight" ,rust-tree-sitter-highlight-0.20)
                         ("rust-tree-sitter-rust" ,rust-tree-sitter-rust-0.20)
                         ("rust-unicode-segmentation" ,rust-unicode-segmentation-1)
                         ("rust-uuid" ,rust-uuid-1))
         #:phases ,#~(modify-phases %standard-phases
                       (replace 'install
                         (lambda _
                           (let* ((bin (string-append #$output "/bin")))
                             (install-file "target/release/ktsctl" bin)
                             (install-file "target/release/kak-tree-sitter" bin)))))))
      (home-page "https://git.sr.ht/~hadronized/kak-tree-sitter/")
      (synopsis "Server between Kakoune and tree-sitter")
      (description
       "This package provides a server between Kakoune and tree-sitter.")
      (license license:bsd-3))))

(define-public kak-snippets
  (let ((commit "a0016bb0b0aac19db0740aaef869ff46233dbed5")
        (revision "0"))
    (package
      (name "kak-snippets")
      (version (git-version "0.0.0" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/occivink/kakoune-snippets")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32 "04hndw91a3wy2z224a68y2cwqdv0jy6iq5dvw9v92d0j8n1v9f8x"))))
     (build-system copy-build-system)
     (arguments
       (list #:install-plan
             #~'(("snippets.kak"
                  "share/kak/autoload/plugins/kakoune-snippets/"))))
     (home-page "https://github.com/occivink/kakoune-snippets")
     (synopsis "Snippet support for kakoune")
     (description "This package provides snippet generation functionality for
Kakoune.")
     (license license:unlicense))))
