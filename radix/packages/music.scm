(define-module (radix packages music)
  #:use-module (guix build-system gnu)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages algebra)
  #:use-module (gnu packages audio)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages commencement)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages image)
  #:use-module (gnu packages image-viewers)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages mp3)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages stb)
  #:use-module (gnu packages video)
  #:use-module (gnu packages xiph)
  #:use-module (radix packages audio))

;; Thanks Pavel :)
(define-public kew
  (package
   (name "kew")
   (version "3.0.2")
   (source
     (origin (method git-fetch)
             (uri (git-reference
                    (url "https://github.com/ravachol/kew")
                    (commit (string-append "v" version))))
             (file-name (git-file-name name version))
             (sha256
               (base32 "02shbzw7izh3yw4f38v1xin4l6hlxm0xg15im28sw2cd22ikaq7q"))))
   (build-system gnu-build-system)
   (arguments
     (list #:tests? #f
           #:make-flags
           #~(let ((chafa #$(this-package-native-input "chafa"))
                   (gcc-toolchain #$(this-package-native-input "gcc-toolchain"))
                   (gdk-pixbuf #$(this-package-native-input "gdk-pixbuf"))
                   (glib #$(this-package-native-input "glib"))
                   (opusfile #$(this-package-native-input "opusfile"))
                   (stb-image #$(this-package-native-input "stb-image"))
                   (stb-image-resize2 #$(this-package-native-input "stb-image-resize2")))
               (list (string-append "PREFIX=" (assoc-ref %outputs "out"))
                     (string-append "CFLAGS="
                                     "-fPIE "
                                     "-I" chafa "/include/chafa "
                                     "-I" chafa "/lib/chafa/include "
                                     "-I" gcc-toolchain "/include "
                                     "-I" gdk-pixbuf "/include/gdk-pixbuf-2.0 "
                                     "-I" glib "/include/glib-2.0 "
                                     "-I" glib "/lib/glib-2.0/include "
                                     "-I" opusfile "/include/opus "
                                     "-I" stb-image "/include "
                                     "-I" stb-image-resize2 "/include ")))
            #:phases
            #~(modify-phases %standard-phases
                (delete 'configure)
                (add-after 'install 'wrap-kew
                    (lambda* (#:key outputs inputs #:allow-other-keys)
                      (let ((out (assoc-ref outputs "out"))
                            (alsa-lib (assoc-ref inputs "alsa-lib")))
                        (wrap-program (string-append out "/bin/kew")
                          `("LD_LIBRARY_PATH" ":" prefix
                            (,(string-append alsa-lib "/lib"))))))))))
   (native-inputs
     (list bash-minimal
           chafa
           fftwf
           gcc-toolchain
           gdk-pixbuf
           glib
           libogg
           miniaudio
           opusfile
           pkg-config
           stb-image
           stb-image-resize2
           taglib))
   (inputs
     (list alsa-lib
           dbus
           faad2
           freeimage
           libvorbis
           opus))
   (synopsis "A command-line music player.")
   (description "A command-line music player.")
   (home-page "https://github.com/ravachol/kew")
   (license license:gpl2)))
