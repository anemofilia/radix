(define-module (radix packages emacs-xyz)
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix build-system emacs)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (gnu packages emacs-xyz))

(define-public emacs-lean4-mode
  (let ((commit "da7b63d854d010d621e2c82a53d6ae2d94dd53b0")
        (revision "0"))
    (package
      (name "emacs-lean4-mode")
      (version (git-version "0.0.0" revision commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://github.com/leanprover/lean4-mode/")
                      (commit commit)))
                (file-name (git-file-name name version))
                (sha256
                 (base32 "1va1ini3cbsmdrbab17kiwbn2h6b0jc8lb3vabmmazsrq0hhk8sk"))))
      (inputs (list emacs-flycheck
                    emacs-lsp-mode
                    emacs-magit))
      (build-system emacs-build-system)
      (synopsis "Lean4 Emacs mode")
      (description "Lean4 Emacs mode for the interactive proof assistant Lean4")
      (home-page "https://github.com/leanprover/lean4-mode")
      (license license:asl2.0))))

(define-public emacs-mastodon
  (package
    (name "emacs-mastodon")
    (version "1.0.23")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://codeberg.org/rougier/mastodon.el")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1jknnp66fn6prk3b1xjjbml798vczfy7agf72pcvwrciljm9ij34"))))
    (build-system emacs-build-system)
    (arguments
     (list #:phases
           #~(modify-phases %standard-phases
               ;; Move the source files to the top level, which is
               ;; included in the EMACSLOADPATH.
               (add-after 'unpack 'move-source-files
                 (lambda _ (let ((el-files (find-files "./lisp" ".*\\.el$")))
                            (for-each (lambda (f) (rename-file f (basename f)))
                                      el-files)))))))
    (propagated-inputs (list emacs-request emacs-ts emacs-persist))
    (home-page "https://codeberg.org/rougier/mastodon.el")
    (synopsis "Emacs client for Mastodon")
    (description "@code{mastodon.el} is an Emacs client for Mastodon, the
federated microblogging social network.")
    (license license:gpl3+)))

(define-public emacs-hl-fill-column
  (let ((commit "937695dd80a8e990774ac9f8f1d605c1a6bb2889")
        (revision "0"))
    (package
      (name "emacs-hl-fill-column")
      (version (git-version "0" revision commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://github.com/laishulu/hl-fill-column")
                      (commit commit)))
                (sha256
                 (base32
                  "0j3ja1pxrqzc91w1bclppspv1dhaar8kcqn72vwsxzrz49ldss4h"))))
      (build-system emacs-build-system)
      (home-page "https://github.com/laishulu/hl-fill-column")
      (synopsis "Highlight fill columns for emacs.")
      (description
       "This Emacs package provides two modes to highlight fill columns.")
      (license license:gpl3))))

(define-public emacs-selected
  (let ((commit "1ca6e12f456caa1dc97c3d68597598662eb5de9a")
        (revision "0"))
    (package
      (name "emacs-selected")
      (version (git-version "0" revision commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://github.com/Kungsgeten/selected.el")
                      (commit commit)))
                (sha256
                 (base32
                  "11xjv861mx15fhvv8g67ahri5p8mplnhq9sf30c825bk1f0h7xzy"))))
      (build-system emacs-build-system)
      (home-page "https://github.com/Kungsgeten/selected.el")
      (synopsis "Keymap for when region is active.")
      (description
       "This Emacs package provides the selected-minor-mode for Emacs. When
selected-minor-mode is active, the keybindings in selected-keymap will be
enabled when the region is active. This is useful for commands that operates
on the region, which you only want bound to a key when the region is active.
selected.el also provides selected-global-mode, if you want selected-minor-mode
in every buffer.")
      (license license:expat))))

(define-public emacs-lyrics-fetcher
  (package
    (name "emacs-lyrics-fetcher")
    (version "0.2.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/SqrtMinusOne/lyrics-fetcher.el")
                    (commit version)))
              (sha256
               (base32
                "10lifif5nbbn172l6dyifm00q3ak91bp143ng3p2j5518vah2cb2"))))
    (build-system emacs-build-system)
    (propagated-inputs (list emacs-f emacs-emms emacs-request))
    (home-page "https://github.com/SqrtMinusOne/lyrics-fetcher.el")
    (synopsis "Fetch song lyrics and album covers. Integrates with EMMS.")
    (description "A package to fetch song lyrics and album covers. Integrates
with EMMS.")
    (license license:gpl3+)))

(define-public emacs-dired-hide-dotfiles
  (package
    (name "emacs-dired-hide-dotfiles")
    (version "0.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/mattiasb/dired-hide-dotfiles")
                    (commit (string-append "v" version))))
              (sha256
               (base32
                "1fpzgmvbgfgl6wdrynlpvvdlbm8npgrmnzfz2133zvf5x3zfzq6r"))))
    (build-system emacs-build-system)
    (home-page "https://github.com/mattiasb/dired-hide-dotfiles")
    (synopsis "Hide dotfiles in dired.")
    (description "A package to hide dotfiles in dired.")
    (license license:gpl3+)))

(define-public emacs-isayt
  (let ((commit "c7d1a16a3a3e8aeb7761303ed7d08bce2ce58b1e")
        (revision "0"))
    (package
      (name "emacs-isayt")
      (version (git-version "0" revision commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://gitlab.com/andreyorst/isayt.el")
                      (commit commit)))
                (sha256
                 (base32
                  "0c431vifw9rcblgh1h489zi9l79z7g8v5na06l97dqncidd4xhid"))))
      (build-system emacs-build-system)
      (home-page "https://gitlab.com/andreyorst/isayt.el")
      (synopsis "Indent S-expressions as you type.")
      (description "A package to indent s-expressions as you type.")
      (license license:gpl3+))))
