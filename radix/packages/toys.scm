(define-module (radix packages toys)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages xdisorg)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix gexp)
  #:use-module (guix utils)
  #:use-module (guix build-system gnu)
  #:use-module ((guix licenses) #:prefix license:))

(define-public wayneko
  (let ((commit "cb3668e42562a5b022a02af099de7faf122c27bf")
        (revision "0"))
   (package
     (name "wayneko")
     (version (git-version "0" revision commit))
     (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://codeberg.org/look/wayneko")
               (commit "cb3668e42562a5b022a02af099de7faf122c27bf")))
        (sha256
         (base32
          "1vk7b4z4z6alksglw6jnp8rh5qz90h0sm77031iwwg8ha9kp1n9s"))))
     (build-system gnu-build-system)
     (arguments
       (list #:tests? #f                         ; no tests
             #:make-flags
             #~(list (string-append "CC=" #$(cc-for-target))
                     (string-append "PREFIX=" #$output))
             #:phases
             #~(modify-phases %standard-phases
                 (delete 'configure))))
     (native-inputs
       (list pixman
             pkg-config
             wayland))
     (home-page "https://codeberg.org/look/wayneko")
     (synopsis "The cute oneko cat chasing your mouse pointer, but now on
wayland.")
     (description "The cute oneko cat chasing your mouse pointer, but now on
wayland.")
     (license license:gpl3))))
