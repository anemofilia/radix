(define-module (radix packages terminals)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages ncurses)
  #:use-module (gnu packages terminals)
  #:use-module (guix build-system go)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (guix utils))

(define-public fzf
  (package
    (inherit go-github-com-junegunn-fzf)
    (name "fzf")
    (arguments
     (ensure-keyword-arguments
      (package-arguments go-github-com-junegunn-fzf)
      `(#:install-source? #f
        #:phases
        (modify-phases %standard-phases
          (add-after 'install 'copy-binaries
            (lambda* (#:key outputs #:allow-other-keys)
              (let ((out (assoc-ref outputs "out")))
                (with-directory-excursion "src/github.com/junegunn/fzf"
                  (install-file "bin/fzf-tmux"
                                (string-append out "/bin"))))))
          (add-after 'install 'install-doc
            (lambda* (#:key outputs import-path #:allow-other-keys)
              (display import-path)
              (newline)
              (let* ((out (assoc-ref outputs "out")))
                (install-file
                  (string-append "src/" import-path "/man/man1/fzf.1")
                  (string-append out "/share/man/man1"))
                (install-file
                  (string-append "src/" import-path "/man/man1/fzf-tmux.1")
                  (string-append out "/share/man/man1")))))
          (add-after 'copy-binaries 'wrap-programs
            (lambda* (#:key outputs inputs #:allow-other-keys)
              (let* ((out (assoc-ref outputs "out"))
                     (bin (string-append out "/bin"))
                     (findutils (assoc-ref inputs "findutils"))
                     (ncurses (assoc-ref inputs "ncurses")))
                (wrap-program (string-append bin "/fzf")
                  `("PATH" ":" prefix (,(string-append findutils "/bin"))))
                (wrap-program (string-append bin "/fzf-tmux")
                  `("PATH" ":" prefix (,(string-append ncurses "/bin")))))))
          (add-after 'install 'install-completions
            (lambda* (#:key outputs #:allow-other-keys)
              (let* ((out (assoc-ref outputs "out"))
                     (bash-completion (string-append out "/etc/bash_completion.d"))
                     (fish-functions
                       (string-append out "/share/fish/vendor_functions.d"))
                     (zsh-completion (string-append out "/share/zsh/site-functions")))
                (with-directory-excursion "src/github.com/junegunn/fzf"
                  (mkdir-p bash-completion)
                  (copy-file "shell/completion.bash"
                             (string-append bash-completion "/fzf"))
                  (mkdir-p fish-functions)
                  (copy-file "shell/key-bindings.fish"
                             (string-append fish-functions "/fzf_key_bindings.fish"))
                  (mkdir-p zsh-completion)
                  (copy-file "shell/completion.zsh"
                             (string-append zsh-completion "/_fzf"))))))))))
    (inputs
     `(,@(package-inputs go-github-com-junegunn-fzf)
       ("bash" ,bash-minimal) ; for wrap-program
       ("findutils" ,findutils)
       ("ncurses" ,ncurses)))))
