(define-module (radix packages qt)
  #:use-module (gnu packages)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages vulkan)
  #:use-module (gnu packages markup)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages xorg)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system qt)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix search-paths)
  #:use-module (guix utils)
  #:use-module ((guix licenses) #:prefix license:))

(define qt-url
  (@@ (gnu packages qt) qt-url))

(define qt6ct
  (package
    (name "qt6ct")
    (version "1.6")
    (source
     (origin
       (method url-fetch)
       (uri
        (string-append "mirror://sourceforge/qt6ct/qt6ct-" version ".tar.bz2"))
       (sha256
        (base32 "14742vs32m98nbfb6mad0i8ciff6f46gfcb6v03p4hh2dvhhqgfn"))))
    (build-system qt-build-system)
    (arguments
     (list
      #:tests? #f                      ; No target
      #:phases
      #~(modify-phases %standard-phases
          (add-after 'unpack 'patch
            (lambda _
              (substitute* '("src/qt6ct-qtplugin/CMakeLists.txt"
                             "src/qt6ct-style/CMakeLists.txt")
                (("\\$\\{PLUGINDIR\\}")
                 (string-append #$output "/lib/qt6/plugins"))))))))
    (native-inputs
     (list qttools))
    (inputs
     (list qtsvg))
    (synopsis "Qt6 Configuration Tool")
    (description "Qt6CT is a program that allows users to configure Qt6
settings (such as icons, themes, and fonts) in desktop environments or
window managers, that don't provide Qt integration by themselves.")
    (home-page "https://qt6ct.sourceforge.io/")
    (license license:bsd-2)))

(define-public qtspeech
  (package
    (inherit qtsvg)
    (name "qtspeech")
    (version "6.5.2")
    (source (origin
              (method url-fetch)
              (uri (qt-url name version))
              (sha256
               (base32
                "1cnlc9x0wswzl7j2imi4kvs9zavs4z1mhzzfpwr6d9zlfql9rzw8"))))
   (arguments
     (list
      #:configure-flags #~(list "-DQT_BUILD_TESTS=ON")
      #:phases
      #~(modify-phases %standard-phases
          (add-before 'check 'set-display
            (lambda _
              ;; Make Qt render "offscreen", required for tests.
              (setenv "QT_QPA_PLATFORM" "offscreen"))))))
    (inputs (list qtbase))
    (native-inputs (list perl qtdeclarative qtmultimedia qtxmlpatterns))
    (synopsis "Qt Speech module")
    (description "The Qt Speech module enables a Qt application to support
accessibility features such as text-to-speech, which is useful for end-users
who are visually challenged or cannot access the application for whatever
reason.  The most common use case where text-to-speech comes in handy is when
the end-user is driving and cannot attend the incoming messages on the phone.
In such a scenario, the messaging application can read out the incoming
message.")))

(define qt3d
  (package
    (inherit qtbase)
    (name "qt3d")
    (version "6.6.0")
    (source
     (origin
       (method url-fetch)
       (uri (qt-url name version))
       (sha256
        (base32
         "0apwq6cqxn1xszhaawrz14yyy9akbmh6i5yys3v74kbz4537ma0d"))))
    (propagated-inputs `())
    (native-inputs (list perl))
    (inputs (list mesa qtbase-5 vulkan-headers zlib))
    (arguments
     (list #:phases
           #~(modify-phases %standard-phases
               (add-before 'configure 'configure-qmake
                 (lambda* (#:key inputs outputs #:allow-other-keys)
                   (let* ((tmpdir (string-append (getenv "TMPDIR")))
                          (qmake (string-append tmpdir "/qmake"))
                          (qt.conf (string-append tmpdir "/qt.conf")))
                     (symlink (which "qmake") qmake)
                     (setenv "PATH"
                             (string-append tmpdir ":"
                                            (getenv "PATH")))
                     (with-output-to-file qt.conf
                       (lambda ()
                         (format #t "[Paths]
                                     Prefix=~a~
                                     ArchData=lib/qt5~
                                     Data=share/qt5~
                                     Documentation=share/doc/qt5~
                                     Headers=include/qt5~
                                     Libraries=lib~
                                     LibraryExecutables=lib/qt5/libexec~
                                     Binaries=bin~
                                     Tests=tests~
                                     Plugins=lib/qt5/plugins~
                                     Imports=lib/qt5/imports~
                                     Qml2Imports=lib/qt5/qml~
                                     Translations=share/qt5/translations~
                                     Settings=etc/xdg~
                                     Examples=share/doc/qt5/examples~
                                     HostPrefix=~a~
                                     HostData=lib/qt5~
                                     HostBinaries=bin~
                                     HostLibraries=lib~

                                     [EffectiveSourcePaths]~
                                     HostPrefix=~a~
                                     HostData=lib/qt5"
                                 #$output #$output #$(this-package-input
                                                      "qtbase")))))))
               (replace 'configure
                 (lambda* (#:key inputs outputs #:allow-other-keys)
                   (invoke "qmake"
                           "QT_BUILD_PARTS = libs tools tests")))
               (add-before 'check 'set-display
                 (lambda _
                   (setenv "QT_QPA_PLATFORM" "offscreen"))))))
    (synopsis "Qt module for 3D")
    (description "The Qt3d module provides classes for displaying 3D.")))

qt3d
