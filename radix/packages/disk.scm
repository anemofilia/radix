(define-module (radix packages disk)
  #:use-module (gnu packages golang)
  #:use-module (gnu packages golang-xyz)
  #:use-module (gnu packages golang-build)
  #:use-module (guix packages)
  #:use-module (guix build-system go)
  #:use-module (guix git-download)
  #:use-module (guix gexp)
  #:use-module ((guix licenses) #:prefix license:)

  #:replace (lf))

(define-public lf
  (let* ((import-path "github.com/gokcehan/lf")
         (lf-url (string-append "https://" import-path)))
    (package
      (name "lf")
      (version "33")
      (source
        (origin
          (method git-fetch)
          (uri (git-reference
                 (url lf-url)
                 (commit (string-append "r" version))))
          (file-name (git-file-name name version))
          (sha256
            (base32 "1jmqf27ysi35n3hqahlzs5hym7i4w1mplklrvv0lc0baddzx7av8"))))
      (build-system go-build-system)
      (arguments
        (list #:go go-1.18
              #:install-source? #f
              #:import-path import-path
              #:phases
              #~(let ((src (string-append "src/" #$import-path))
                      (man1 (string-append #$output "/share/man/man1"))
                      (applications (string-append #$output "/share/applications")))
                  (modify-phases %standard-phases
                    (add-after 'unpack 'install-man-page
                      (lambda _
                        (install-file (string-append src "/lf.1")
                                      man1)))
                    (add-after 'unpack 'install-desktop-entry
                      (lambda _
                        (install-file (string-append src "/lf.desktop")
                                      applications)))))))
      (inputs (list go-golang-org-x-term
                    go-golang-org-x-sys
                    go-github-com-mattn-go-runewidth
                    go-github-com-fsnotify-fsnotify
                    go-github-com-gdamore-tcell-v2
                    go-github-com-djherbis-times))
      (home-page lf-url)
      (synopsis "Console file browser similar to Ranger")
      (description "lf (as in \"list files\") is a terminal file manager
written in Go.  It is heavily inspired by ranger with some missing and extra
features.  Some of the missing features are deliberately omitted since they are
better handled by external tools.")
      (license license:expat))))
