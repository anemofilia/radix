(define-module (radix packages tex)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages pdf)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages terminals)
  #:use-module (guix build-system qt)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module ((guix licenses)
                #:prefix license:))

(define-public texstudio
  (package
    (name "texstudio")
    (version "4.8.4")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/texstudio-org/texstudio")
                    (commit version)))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0gkrbkrn86g7gj3cjbvw55czrg979czb7ib5w0vlic8frmwgsql0"))))
    (build-system qt-build-system)
    (arguments
     (list #:tests? #f))                    ;tests work only with debug build
    (native-inputs
     (list pkg-config
           poppler-qt5
           qtdeclarative-5
           qtsvg-5
           qttools-5))
    (home-page "https://www.texstudio.org/")
    (synopsis "Feature-packed LaTeX editor")
    (description
     "TeXstudio is an integrated writing environment for creating LaTeX
documents.  It makes writing LaTeX comfortable by providing features such as
syntax-highlighting, an integrated viewer and reference checking.")
    (license license:gpl3)))

texstudio
