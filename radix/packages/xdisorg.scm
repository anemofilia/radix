(define-module (radix packages xdisorg)
  #:use-module (gnu packages)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages man)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages wm)
  #:use-module (gnu packages xdisorg)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system meson)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module ((radix packages) #:prefix radix:))

(define-public wlrctl
  (package
    (name "wlrctl")
    (version "0.2.2")
    (source (origin
             (method git-fetch)
             (uri (git-reference
                   (url "https://git.sr.ht/~brocellous/wlrctl")
                   (commit "693bf26c06b65b8161028378a08a2b454b3054eb")))
             (sha256
              (base32 "1wba07avg4gbcxd05ap4z9lbjckypksb9zmg31vy87fp8dkmv4jh"))))
   (build-system meson-build-system)
   (native-inputs
     (list cmake
           pkg-config
           libxkbcommon
           scdoc
           wlroots))
   (home-page "https://git.sr.ht/~brocellous/wlrctl")
   (synopsis "wlrctl is a command line utility for miscellaneous wlroots
Wayland extensions.")
   (description "wlrctl is a command line utility for miscellaneous wlroots
Wayland extensions.")
   (license license:expat)))

(define-public fyi
  (package
    (name "fyi")
    (version "1.0.2")
    (source (origin
             (method git-fetch)
             (uri (git-reference
                   (url "https://codeberg.org/dnkl/fyi")
                   (commit version)))
             (file-name (git-file-name name version))
             (sha256
              (base32
               "1cbqx659fzfnjdhvimwng7h4yx35rm9aldp8jq75hqxf1lqm7xb6"))))
    (build-system meson-build-system)
    (native-inputs
      (list cmake
            pkg-config
            scdoc))
    (inputs
      (list dbus))
    (home-page "https://codeberg.org/dnkl/fyi")
    (synopsis "notify-send alternative.")
    (description "FYI (for your information) is a command line utility to send
desktop notifications to the user via a notification daemon implementing XDG
desktop notifications.")
    (license license:expat)))

(define-public fuzzel-lowercase
  (package
    (inherit fuzzel)
    (source
      (let ((fuzzel-origin (package-source fuzzel)))
        (origin
          (inherit fuzzel-origin)
          (patches (append (origin-patches fuzzel-origin)
                           (parameterize ((%patch-path radix:%patch-path))
                             (search-patches "fuzzel-lowercase.patch")))))))
    (description
     (format #f "~a~%~%This version provides a consistent display of
                 application names by rendering them all in lowercase."
               (package-description fuzzel)))))
