(define-module (radix packages package-management)
  #:use-module (gnu packages package-management)
  #:use-module (guix build-system trivial)
  #:use-module (guix gexp)
  #:use-module (guix packages))

(define-public (current-system-guix)
  (let* ((current-system-guix
          (readlink "/run/current-system/profile/bin/guix"))
         (current-guix-profile
          (dirname (dirname current-system-guix))))
    (package/inherit guix
     (build-system trivial-build-system)
     (arguments (list #:builder
                      #~(symlink #$current-guix-profile
                                 #$output))))))
