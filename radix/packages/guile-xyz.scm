(define-module (radix packages guile-xyz)
  #:use-module (gnu packages guile)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages gnupg)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages pkg-config)
  #:use-module (guix build-system gnu)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:))

(define-public guile-oauth
  (package
    (name "guile-oauth")
    (version "1.3.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/aconchillo/guile-oauth")
               (commit version)))
        (file-name (git-file-name name version))
        (sha256
          (base32 "002hgs4xfrrz0rqa6n1078cn7vz5f70azw1kpljvb4dmv228gfxq"))))
    (build-system gnu-build-system)
    (arguments
      `(#:make-flags '("GUILE_AUTO_COMPILE=0")))
    (native-inputs (list guile-3.0
                         pkg-config
                         autoconf
                         automake))
    (inputs (list guile-gnutls
                  guile-gcrypt
                  guile-json-4))
    (home-page "https://github.com/aconchillo/guile-oauth")
    (synopsis "OAuth module for Guile")
    (description "guile-oauth is an OAuth client module for Guile")
    (license license:gpl3+)))
