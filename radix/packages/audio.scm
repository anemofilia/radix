(define-module (radix packages audio)
  #:use-module (guix build-system copy)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module ((guix licenses) #:prefix license:))

(define-public miniaudio
  (package
    (name "miniaudio")
    (version "0.11.21")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/mackron/miniaudio")
                    (commit version)))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0hlqp44ifi3vswvfpqljj89321y6yz3zq6rng51wfq8p4n9k5qhh"))))
   (build-system copy-build-system)
   (arguments
       (list #:install-plan
             #~'(("miniaudio.h" "include/")
                 ("extras/miniaudio_libopus.h" "include/")
                 ("extras/miniaudio_libvorbis.h" "include/"))))
   (home-page "https://github.com/mackron/miniaudio")
   (synopsis "Audio playback and capture library written in C, in a single
source file.")
   (description "miniaudio is written in C with no dependencies except the
standard library and should compile clean on all major compilers without the
need to install any additional development packages. All major desktop
and mobile platforms are supported.

Features:
@itemize
@item Simple build system with no external dependencies.
@item Simple and flexible API.
@item Low-level API for direct access to raw audio data.
@item High-level API for sound management, mixing, effects and optional 3D spatialization.
@item Flexible node graph system for advanced mixing and effect processing.
@item Resource management for loading sound files.
@item Decoding, with built-in support for WAV, FLAC and MP3, in addition to being able to plug in custom decoders.
@item Encoding (WAV only).
@item Data conversion.
@item Resampling, including custom resamplers.
@item Channel mapping.
@item Basic generation of waveforms and noise.
@item Basic effects and filters.
@end itemize")
   ;; The user can choose either license.
   (license (list license:unlicense
                  license:expat))))
