(define-module (radix packages linux)
  #:use-module (gnu packages crates-io)
  #:use-module (gnu packages crates-windows)
  #:use-module (gnu packages linux)
  #:use-module (guix build-system cargo)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (radix packages crates-io))

(define-public thinkfan-next
  (package
    (inherit thinkfan)
    (arguments
     (list #:modules '((guix build cmake-build-system)
                       (guix build utils)
                       (srfi srfi-26))
           #:tests? #f
           #:configure-flags #~(list "-DUSE_ATASMART:BOOL=ON")
           #:phases #~(modify-phases %standard-phases
                        (add-after 'unpack 'create-init-scripts
                          (lambda _
                            (substitute* "CMakeLists.txt"
                              (("/etc" directory)
                               (string-append #$output directory))))))))))

(define-public bustd
  (package
    (name "bustd")
    (version "0.1.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "bustd" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0byz7ml2vcdgmdxw1kmdbja83541w2pwcri82kfc4fdi8fr3100j"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-argh" ,rust-argh-0.1)
                       ("rust-cc" ,rust-cc-1)
                       ("rust-cfg-if" ,rust-cfg-if-1)
                       ("rust-daemonize" ,rust-daemonize-0.4)
                       ("rust-glob" ,rust-glob-0.3)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-memchr" ,rust-memchr-2)
                       ("rust-procfs" ,rust-procfs-0.9))))
    (home-page "https://github.com/vrmiguel/bustd")
    (synopsis "Lightweight process killer daemon for out-of-memory scenarios")
    (description
     "This package provides Lightweight process killer daemon for out-of-memory
scenarios.")
    (license license:expat)))
