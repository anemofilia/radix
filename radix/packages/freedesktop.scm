(define-module (radix packages freedesktop)
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix build-system copy)
  #:use-module (guix build-system meson)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages man)
  #:use-module (gnu packages pkg-config))

(define-public xdg-terminal-exec
  (package
    (name "xdg-terminal-exec")
    (version "0.9.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://github.com/Vladimir-csp/xdg-terminal-exec")
              (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
         (base32 "05a8gy8dg18cb0bw2m0nfaxn18pdcb9qipscya58x12x29z8lmgf"))))
   (build-system copy-build-system)
   (arguments
     (list #:install-plan #~'(("xdg-terminal-exec" "bin/"))))
   (home-page "https://github.com/Vladimir-csp/xdg-terminal-exec")
   (synopsis "Proposal for XDG terminal execution utility.")
   (description "Proposal for XDG terminal execution utility.")
   (license license:gpl3)))

(define-public xdg-desktop-portal-termfilechooser
  (let ((url "https://github.com/hunkyburrito/xdg-desktop-portal-termfilechooser")
        (commit "2bc7343c655d0705227e41088ce3fbc7acc12066")
        (revision "0"))
    (package
      (name "xdg-desktop-portal-termfilechooser")
      (version (git-version "0.0.0" revision commit))
      (source
        (origin
          (method git-fetch)
          (uri (git-reference
                (url url)
                (commit commit)))
          (file-name (git-file-name name version))
          (sha256
           (base32 "1c8qij01zc6xfcrq93davnw0bk7r7rj61f91sc7m84vq5w10yxvn"))))
     (build-system meson-build-system)
     (native-inputs
       (list cmake-minimal
             libinih
             pkg-config
             scdoc))
     (inputs
       (list basu))
     (propagated-inputs
       (list xdg-desktop-portal))
     (home-page url)
     (synopsis "xdg-desktop-portal backend for choosing files with your favorite
file chooser")
     (description "xdg-desktop-portal backend for choosing files with your
favorite file chooser. By default, it will use the yazi file manager, but this
is customizable.")
     (license license:expat))))
