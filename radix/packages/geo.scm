(define-module (radix packages geo)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages ncurses)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages sdl)
  #:use-module (gnu packages web)
  #:use-module (guix build-system zig)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix utils))

(define-public mepo
 (package
  (name "mepo")
  (version "1.3.3")
  (source (origin
           (method git-fetch)
           (uri (git-reference
                 (url "https://git.sr.ht/~mil/mepo")
                 (commit version)))
           (file-name (git-file-name name version))
           (sha256
            (base32 "025nxkilar3gdif2f1zsiy27614x2hbpcmh38sl61ng37aji0jw4"))))
  (build-system zig-build-system)
  (arguments
    (list #:install-source? #f
          #:zig-release-type "safe"
          #:zig-build-flags
          #~(list "--search-prefix"
                  #$(this-package-input "curl"))
          #:phases
          #~(modify-phases %standard-phases
              (add-after 'install 'install-desktop-file
                (lambda _
                  (install-file "assets/mepo.desktop"
                                (string-append #$output
                                               "/share/applications")))))))
  (native-inputs
    (list pkg-config))
  (inputs
    (list curl jq ncurses sdl2 sdl2-gfx sdl2-image sdl2-ttf))
  (synopsis "Fast, simple, and hackable OSM map viewer for Linux. Designed with
the Pinephone & mobile linux in mind; works both offline and online.")
  (description "Fast, simple, and hackable OSM map viewer for Linux. Designed
with the Pinephone & mobile linux in mind; works both offline and online.")
  (home-page "https://git.sr.ht/~mil/mepo")
  (license license:gpl3+)))
