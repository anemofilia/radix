(define-module (radix packages seninha)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system copy)
  #:use-module ((guix build utils) #:hide (delete))
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages imagemagick)
  #:use-module (gnu packages pdf)
  #:use-module (gnu packages video)
  #:use-module (gnu packages xorg))

(define-public shod
  (package
    (name "shod")
    (version "2.6.2")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://github.com/phillbush/shod/"
                                  "archive/refs/tags/v" version ".tar.gz"))
              (sha256
               (base32
                "1ambmkjpcgx4hlra6dn428m811n57dm17sry8gn9ms3f7m59r45z"))))
   (build-system gnu-build-system)
   (arguments
    (list
      #:tests? #f
      #:phases
      #~(modify-phases %standard-phases
          (delete 'configure)
          (add-after 'build 'install-xsession
           (lambda _
             ;; Add a .desktop file to xsessions.
             (let ((xsessions (string-append #$output "/share/xsessions")))
               (mkdir-p xsessions)
               (make-desktop-entry-file
                (string-append xsessions "/shod.desktop")
                #:name "shod"
                #:exec (string-append #$output "/bin/shod")
                #:try-exec (string-append #$output "/bin/shod")
                #:comment `((#f ,#$(package-synopsis this-package)))))))) 
      #:make-flags
      #~(list 
          (string-append "PREFIX=" (assoc-ref %outputs "out"))
          (string-append "CC=" #$(cc-for-target))
          (string-join
            (list "CFLAGS=-D_POSIX_C_SOURCE=200809L"
                  "-DGNU_SOURCE"
                  (string-append "-L" (assoc-ref %build-inputs "libx11")
                                      "/lib")
                  (string-append "-L" (assoc-ref %build-inputs "libxft")
                                      "/lib")
                  (string-append "-L" (assoc-ref %build-inputs "libxrandr")
                                      "/lib")
                  (string-append "-L" (assoc-ref %build-inputs "libxpm")
                                      "/lib")
                  (string-append "-L" (assoc-ref %build-inputs "libxrender")
                                      "/lib")
                  (string-append "-I" (assoc-ref %build-inputs "freetype")
                                      "/include/freetype2"))
            " "))))
   (native-inputs 
    (list fontconfig
          freetype
          libx11
          libxft
          libxpm
          libxrandr
          libxrender))
   (home-page "https://github.com/phillbush/shod")
   (synopsis "mouse-based window manager that can tile windows inside floating
containers")
   (description "Shod is a mouse-based window manager that can tile windows
inside floating containers, developed by one of the greatest devs of our era.")
   (license license:expat)))


(define-public fmutils
  (let ((commit "919b32fbc47d0303d59cdca66bfc6f0a8906a0ae")
        (revision "0"))
    (package
      (name "fmutils")
      (version (git-version "0.0.0" revision commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://github.com/phillbush/fmutils")
                      (commit commit)))
                (file-name (git-file-name name version))
                (sha256
                 (base32
                  "1ikn2y1bj8gg045nq7s9g9fc1619pabv3c5lljxxv5bmrlmaxvh4"))))
      (build-system copy-build-system)
      (arguments
       (list #:install-plan #~'(("bulkrename" "bin/")
                                ("pick" "bin/")
                                ("preview" "bin/")
                                ("skel" "bin/")
                                ("trash" "bin/")
                                ("untrash" "bin/")
                                ("unpack" "bin/")
                                ("bulkrename.1" "share/man/man1/")
                                ("pick.1" "share/man/man1/")
                                ("preview.1" "share/man/man1/")
                                ("skel.1" "share/man/man1/")
                                ("trash.1" "share/man/man1/")
                                ("unpack.1" "share/man/man1/"))))
      (home-page "https://github.com/void-linux/void-runit")
      (synopsis "File management utilities in POSIX shell scripts.")
      (description "\
fmutils is a set of public domain POSIX scripts implementing some file
management related utilities.  There are man pages for each script.

The scripts are the following.
@itemize
@item bulkrename: Bulk rename files with your text editor.
@item pick:       Interactivelly select options.
@item preview:    Generate text preview of a file to be used by lf(1) or ranger(1).
@item skel:       Create a template file in the current directory.
@item trash:      Move files to trash following XDG trash specification.
@item untrash:    Remove files from trash following XDG trash specification.
@item unpack:     Unarchive archived files (.zip, .rar, .tar.gz, etc).
@end itemize")
      (license license:public-domain))))

(define-public xfiles
  (package
    (name "xfiles")
    (version "1.1.1")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://github.com/phillbush/xfiles"
                                  "/archive/refs/tags/v" version ".tar.gz"))
              (sha256
               (base32
                "0c3hzw6fyxsfnyaqpqz35z606yik4vj7x274f370303p5by2bzj7"))))
   (build-system gnu-build-system)
   (arguments
    (list
      #:tests? #f
      #:phases
      #~(modify-phases %standard-phases
          (delete 'configure)
          (add-after 'build 'install-scripts
            (lambda _
              (mkdir (string-append #$output "/bin"))
              (copy-file "examples/xfilesthumb"
                         (string-append #$output "/bin/xfilesthumb"))
              (copy-file "examples/xfilesctl"
                         (string-append #$output "/bin/xfilesctl"))))
          (add-after 'build 'install-xsession
           (lambda _
             ;; Add a .desktop file to xsessions.
             (let ((xsessions (string-append #$output "/share/xsessions")))
               (mkdir-p xsessions)
               (make-desktop-entry-file
                (string-append xsessions "/xfiles.desktop")
                #:name "xfiles"
                #:exec (string-append #$output "/bin/xfiles")
                #:try-exec (string-append #$output "/bin/xfiles")
                #:comment `((#f ,#$(package-synopsis this-package)))))))) 
      #:make-flags
      #~(list 
          (string-append "PREFIX=" (assoc-ref %outputs "out"))
          (string-append "CC=" #$(cc-for-target))
          (string-join
            (list "CFLAGS=-D_POSIX_C_SOURCE=200809L"
                  "-DGNU_SOURCE"
                  (string-append "-L" (assoc-ref %build-inputs "libx11")
                                      "/lib")
                  (string-append "-L" (assoc-ref %build-inputs "libxft")
                                      "/lib")
                  (string-append "-L" (assoc-ref %build-inputs "libxrandr")
                                      "/lib")
                  (string-append "-L" (assoc-ref %build-inputs "libxpm")
                                      "/lib")
                  (string-append "-L" (assoc-ref %build-inputs "libxcursor")
                                      "/lib")
                  (string-append "-L" (assoc-ref %build-inputs "libxext")
                                      "/lib")
                  (string-append "-L" (assoc-ref %build-inputs "libxrender")
                                      "/lib")
                  (string-append "-I" (assoc-ref %build-inputs "freetype")
                                      "/include/freetype2"))
            " "))))
   (propagated-inputs
    (list ffmpegthumbnailer
          imagemagick
          librsvg
          poppler))
   (native-inputs 
    (list fontconfig
          freetype
          libxcursor
          libx11
          libxft
          libxpm
          libxrandr
          libxrender))
   (home-page "https://github.com/phillbush/xfiles")
   (synopsis "Simple and configurable file manager for X11")
   (description "XFiles is a file manager for X11. It can navigate through 
directories, show icons for files, select files, call a command to open files,
generate thumbnails, and call a command to run on right mouse button click.")
   (license license:expat)))
