(define-module (radix home services repl)
  #:use-module (guix gexp)
  #:use-module (gnu services)
  #:use-module (gnu home services shepherd)

  #:export (home-shepherd-repl-service-type))

(define (shepherd-repl-service _)
  (shepherd-service
    (provision '(repl))
    (modules '((shepherd service repl)))
    (free-form #~(repl-service))))

(define home-shepherd-repl-service-type
  (service-type
   (name 'home-shepherd-repl)
   (extensions
    (list (service-extension home-shepherd-service-type
                             (compose list shepherd-repl-service))))
   (default-value #t)
   (description "Run a read-eval-print loop (REPL).")))
