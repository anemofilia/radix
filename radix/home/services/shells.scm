(define-module (radix home services shells)
  #:use-module (gnu services configuration)
  #:use-module (gnu home services utils)
  #:use-module (gnu home services shells)
  #:use-module (gnu home services)
  #:use-module (gnu packages shells)
  #:use-module (guix build utils)
  #:use-module (guix gexp)
  #:use-module (guix monads)
  #:use-module (guix store)
  #:use-module (guix derivations)
  #:use-module (guix packages)
  #:use-module (guix records)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9)
  #:use-module (ice-9 match)
  #:use-module (ice-9 regex)
  #:use-module (radix packages fish-xyz)

  #:export (home-fish-service-type
            home-fish-configuration
            home-fish-extension

            abbreviation

            fish-function

            home-tty-colorscheme-service-type
            home-tty-colorscheme-configuration))

;;;
;;; Fish.
;;;

(define (serialize-fish-aliases field-name val)
  #~(string-append
     #$@(map (match-lambda
               ((key . value)
                #~(string-append "alias '" #$key "' \"" #$value "\"\n"))
               (_ ""))
             val)))

(define-record-type <fish-function>
  (fish-function name)
  fish-function?
  (name fish-function-name))

(define-record-type* <abbreviation>
  abbreviation make-abbreviation
  abbreviation?
  (name      abbreviation-name)       ; string | symbol
  (pattern   abbreviation-pattern     ; string | #f
             (default #f))
  (position  abbreviation-position    ; 'command | 'anywhere | #f
             (default #f))            ;   defaults to "command" -´
  (marker    abbreviation-marker      ; char
             (default #\%))
  (expansion abbreviation-expansion)) ; string | <fish-function>

(define strings?
  (list-of string?))

(define fish-abbreviations?
  (list-of abbreviation?))

(define fish-env-vars? alist?)
(define fish-aliases? alist?)

(define (fish-plugin? x)
  (and (package? x)
       (string-prefix? "fish-" (package-name x))))

(define fish-plugins?
  (list-of fish-plugin?))

(define (serialize-fish-abbreviations field-name val)
  #~(string-append
     #$@(map (match-record-lambda <abbreviation>
               (name pattern position marker expansion)
               #~((@@ (ice-9 format) format) #f
                   "~%abbr --add '~a' \\~%~
                   ~@[     --position ~a \\~%~]~
                   ~@[     --regex '~a' \\~%~]~
                   ~@[     --set-cursor=~a \\~%~]~
                   ~@[     ~a~]~%"
                   '#$name
                   '#$position
                   #$pattern
                   #$marker
                   #$(if (fish-function? expansion)
                       (format #f "--function ~a"
                                  (fish-function-name expansion))
                       (format #f "\"~a\"" expansion))))
             val)))

(define (serialize-fish-env-vars field-name val)
  #~(string-append
     #$@(map (match-lambda
               ((key . #f)
                "")
               ((key . #t)
                #~(string-append "set -x " #$key "\n"))
               ((key . value)
                #~(string-append "set -x " #$key " "  #$(if (strings? value)
                                                          (string-join value)
                                                          value)  "\n")))
             val)))

(define serialize-fish-plugins
  (@@ (gnu services configuration) empty-serializer))

(define-configuration home-fish-configuration
  (package
    (package fish)
    "The Fish package to use.")
  (config
   (text-config '())
   "List of file-like objects, which will be added to
@file{$XDG_CONFIG_HOME/fish/config.fish}.")
  (environment-variables
   (fish-env-vars '())
   "Association list of environment variables to set in Fish.")
  (aliases
   (fish-aliases '())
   "Association list of aliases for Fish, both the key and the value
should be a string.  An alias is just a simple function that wraps a
command, If you want something more akin to @dfn{aliases} in POSIX
shells, see the @code{abbreviations} field.")
  (abbreviations
   (fish-abbreviations '())
   "List of abbreviations for Fish.  These are words that, when
typed in the shell, will automatically expand to the full text.")
  (plugins
   (fish-plugins '())
   "List of plugins for Fish.  These are packages that, provide extra functions,
hooks, and/or abbreviations for fish."))

(define (fish-files-service config)
  `(("fish/config.fish"
     ,(mixed-text-file
       "fish-config.fish"
       #~(string-append "\
# if we haven't sourced the login config, do it
status --is-login; and not set -q __fish_login_config_sourced
and begin

  set --prepend fish_function_path " #$fish-foreign-env "/share/fish/functions
  fenv source $HOME/.profile
  set -e fish_function_path[1]

  set -g __fish_login_config_sourced 1

end\n\n")
       (serialize-configuration
        config
        home-fish-configuration-fields)))))

(define (fish-profile-service config)
  (cons* (home-fish-configuration-package config)
         (home-fish-configuration-plugins config)))

(define-configuration/no-serialization home-fish-extension
  (config
   (text-config '())
   "List of file-like objects for extending the Fish initialization file.")
  (environment-variables
   (alist '())
   "Association list of environment variables to set.")
  (aliases
   (alist '())
   "Association list of Fish aliases.")
  (abbreviations
   (fish-abbreviations '())
   "List of Fish abbreviations.")
  (plugins
   (fish-plugins '())
   "List of plugins for Fish."))

(define (home-fish-extensions original-config extension-configs)
  (home-fish-configuration
   (inherit original-config)
   (config
    (append (home-fish-configuration-config original-config)
            (append-map
             home-fish-extension-config extension-configs)))
   (environment-variables
    (append (home-fish-configuration-environment-variables original-config)
            (append-map
             home-fish-extension-environment-variables extension-configs)))
   (aliases
    (append (home-fish-configuration-aliases original-config)
            (append-map
             home-fish-extension-aliases extension-configs)))
   (abbreviations
    (append (home-fish-configuration-abbreviations original-config)
            (append-map
             home-fish-extension-abbreviations extension-configs)))
   (plugins
    (append (home-fish-configuration-plugins original-config)
            (append-map
             home-fish-extension-plugins extension-configs)))))

(define (fish-plugin-files-service config)
  (define (plugin-files plugin drv)
    (define out (derivation->output-path drv))
    (define dir (string-append out "/share/fish/"))
    (map (lambda (file)
           (let ((file (regexp-substitute #f (string-match dir file)
                                             'pre "fish" 'post)))
             (list file
                   (file-append plugin (string-append "/share/" file)))))
         (find-files dir)))

  (define plugins (home-fish-configuration-plugins config))
  (define derivations
    (with-store store
      (set-build-options store
                         #:print-build-trace #t
                         #:print-extended-build-trace? #t
                         #:multiplexed-build-output? #t)
      (run-with-store store
        (mlet %store-monad
              ((drvs (mapm %store-monad
                           package->derivation
                           plugins)))
          (build-derivations store drvs)
          (return drvs)))))

  (append-map plugin-files plugins derivations))

;; TODO: Support for generating completion files
(define home-fish-service-type
  (service-type (name 'home-fish)
                (extensions
                 (list (service-extension
                        home-xdg-configuration-files-service-type
                        (lambda (config)
                          (append (fish-files-service config)
                                  (fish-plugin-files-service config))))
                       (service-extension
                        home-profile-service-type
                        fish-profile-service)))
                (compose identity)
                (extend home-fish-extensions)
                (default-value (home-fish-configuration))
                (description "\
Install and configure Fish, the friendly interactive shell.")))

(define (generate-home-fish-documentation)
  (string-append
   (generate-documentation
    `((home-fish-configuration
       ,home-fish-configuration-fields))
    'home-fish-configuration)
   "\n\n"
   (generate-documentation
    `((home-fish-extension
       ,home-fish-extension-fields))
    'home-fish-extension)))

(define (color? x)
  (and (equal? (string-length x) 6)
       (char-set-every (lambda (c)
                         (char-set-contains? char-set:hex-digit c))
                       (string->char-set x))))

(define serialize-color empty-serializer)
(define-maybe color)

(define-configuration home-tty-colorscheme-configuration
  (regular-black
    maybe-color
   "The color to be used as regular black.")
  (regular-red
    maybe-color
   "The color to be used as regular red.")
  (regular-green
    maybe-color
   "The color to be used as regular green.")
  (regular-yellow
    maybe-color
   "The color to be used as regular yellow.")
  (regular-blue
    maybe-color
   "The color to be used as regular blue.")
  (regular-magenta
    maybe-color
   "The color to be used as regular magenta.")
  (regular-cyan
    maybe-color
   "The color to be used as regular cyan.")
  (regular-white
    maybe-color
   "The color to be used as regular white.")
  (bright-black
    maybe-color
   "The color to be used as bright black.")
  (bright-red
    maybe-color
   "The color to be used as bright red.")
  (bright-green
    maybe-color
   "The color to be used as bright green.")
  (bright-yellow
    maybe-color
   "The color to be used as bright yellow.")
  (bright-blue
    maybe-color
   "The color to be used as bright blue.")
  (bright-magenta
    maybe-color
   "The color to be used as bright magenta.")
  (bright-cyan
    maybe-color
   "The color to be used as bright cyan.")
  (bright-white
    maybe-color
   "The color to be used as bright white."))

(define (tty-colorscheme-text config)
  (define (unset-value? val)
    (equal? val %unset-value))
  (match-record config <home-tty-colorscheme-configuration>
    (regular-black regular-red regular-green regular-yellow regular-blue
     regular-magenta regular-cyan regular-white bright-black bright-red
     bright-green bright-yellow bright-blue bright-magenta bright-cyan
     bright-white)
    (list (plain-file "tty-colorscheme"
            (string-join
              (list "[ \"$TERM\" = \"linux\" ] && {\\"
                    (if (unset-value? regular-black) ""
                      (string-append "  echo -en \"\\e]P0\"'" regular-black "'"))
                    (if (unset-value? regular-red) ""
                      (string-append "  echo -en \"\\e]P1\"'" regular-red "'"))
                    (if (unset-value? regular-green) ""
                      (string-append "  echo -en \"\\e]P2\"'" regular-green "'"))
                    (if (unset-value? regular-yellow) ""
                      (string-append "  echo -en \"\\e]P3\"'" regular-yellow "'"))
                    (if (unset-value? regular-blue) ""
                      (string-append "  echo -en \"\\e]P4\"'" regular-blue "'"))
                    (if (unset-value? regular-magenta) ""
                      (string-append "  echo -en \"\\e]P5\"'" regular-magenta "'"))
                    (if (unset-value? regular-cyan) ""
                      (string-append "  echo -en \"\\e]P6\"'" regular-cyan "'"))
                    (if (unset-value? regular-white) ""
                      (string-append "  echo -en \"\\e]P7\"'" regular-white "'"))
                    (if (unset-value? bright-black) ""
                      (string-append "  echo -en \"\\e]P8\"'" bright-black "'"))
                    (if (unset-value? bright-red) ""
                      (string-append "  echo -en \"\\e]P9\"'" bright-red "'"))
                    (if (unset-value? bright-green) ""
                      (string-append "  echo -en \"\\e]PA\"'" bright-green "'"))
                    (if (unset-value? bright-yellow) ""
                      (string-append "  echo -en \"\\e]PB\"'" bright-yellow "'"))
                    (if (unset-value? bright-blue) ""
                      (string-append "  echo -en \"\\e]PC\"'" bright-blue "'"))
                    (if (unset-value? bright-magenta) ""
                      (string-append "  echo -en \"\\e]PD\"'" bright-magenta "'"))
                    (if (unset-value? bright-cyan) ""
                      (string-append "  echo -en \"\\e]PE\"'" bright-cyan "'"))
                    (if (unset-value? bright-white) ""
                      (string-append "  echo -en \"\\e]PF\"'" bright-white "'"))
                    "}")
             "\n")))))

(define home-tty-colorscheme-service-type
  (service-type (name 'home-tty-colorscheme)
                (extensions
                 (list (service-extension home-shell-profile-service-type
                                          tty-colorscheme-text)))
                (compose identity)
                (default-value (home-tty-colorscheme-configuration))
                (description "Set tty colorscheme.")))
