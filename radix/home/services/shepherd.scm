(define-module (radix home services shepherd)
  #:use-module (gnu home services)
  #:use-module (gnu services)
  #:use-module (radix services shepherd)

  #:export (radix:home-shepherd-timer-service-type)
  #:re-export (shepherd-timer
               shepherd-timer-name
               shepherd-timer-event
               shepherd-timer-action
               shepherd-timer-log-file
               shepherd-timer-modules
               shepherd-timer-max-duration
               shepherd-timer-occurrences
               shepherd-timer-wait-for-termination?))

(define radix:home-shepherd-timer-service-type
  (service-type
   (inherit (system->home-service-type radix:shepherd-timer-service-type))
   (default-value '())))                          ;requirement
