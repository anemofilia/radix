(define-module (radix home services)
  #:use-module (guix gexp)
  #:use-module (gnu home services)
  #:use-module (gnu services)
  #:use-module (srfi srfi-1)
  #:export (home-directories-service-type
            home-repositories-service-type
            home-symlinks-service-type
            savannah-url
            codeberg-url))

(define home-directories-service-type
  (service-type
   (name 'home-directories)
   (extensions
    (list (service-extension
           home-activation-service-type
           (lambda (subdirectories)
             #~(for-each (lambda (dir)
                           ((@ (guix build utils) mkdir-p)
                            (string-append (getenv "HOME") "/" dir)))
                         '#$subdirectories)))))
   (compose concatenate)
   (extend append)
   (description "Add subdirectories to your home directory.")))

(define (savannah-url name)
  (string-append "https://git.savannah.gnu.org/git/" name))

(define (codeberg-url user-name project-name)
  (string-append "https://codeberg.org/" user-name "/" project-name))

(define home-repositories-service-type
  (service-type
   (name 'home-repositories)
   (extensions
    (list (service-extension
           home-activation-service-type
           (lambda (pairs)
             #~(let ((missing-repository-pairs
                      (filter ((@ (ice-9 match) match-lambda)
                                ((target url)
                                 ((negate file-exists?)
                                  (string-append (getenv "HOME") "/" target))))
                              '#$pairs)))
                 (unless (null? missing-repository-pairs)
                   (format #t "Cloning missing repositories.\n")
                   (for-each
                     ((@ (ice-9 match) match-lambda)
                       ((target url)
                        (let ((target (string-append (getenv "HOME") "/" target)))
                          (unless (file-exists? target)
                            ((@ (guix build utils) mkdir-p) (dirname target))
                            (format #t "Cloning ~a into ~a..." url target)
                            ((@ (git) clone) url target)
                            (format #t " done\n")))))
                     missing-repository-pairs)
                    (format #t "Finished cloning repositories.\n")))))))
   (compose concatenate)
   (extend append)
   (description "Add repositories to your home directory.")))

(define home-symlinks-service-type
  (service-type
   (name 'home-symlinks)
   (extensions
    (list (service-extension
           home-activation-service-type
           (lambda (files)
             #~(for-each
                 ((@ (ice-9 match) match-lambda)
                   ((target file)
                    (let* ((target (string-append (getenv "HOME") "/" target))
                           (pivot (string-append target ".new")))
                      ((@ (guix build utils) mkdir-p) (dirname target))
                      (symlink file pivot)
                      (rename-file pivot target))))
                 '#$files)))))
   (compose concatenate)
   (extend append)
   (description "Add symlinks to your home directory.")))
