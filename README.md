# GNU/Radix

This repository features my [GNU Guix](https://guix.gnu.org/) channel, which contains [Free Software](https://www.gnu.org/philosophy/free-sw.html) only.

Most packages and services here are, at least intended, to be upstreamed.

My dotfiles and personal configurations can be found in [Zero](https://codeberg.org/anemofilia/zero).

## Usage
To update Radix along with Guix via guix pull, write the following definition to ~/.config/guix/channels.scm,

```scheme
(channel
  (name 'radix)
  (url "https://codeberg.org/anemofilia/radix.git")
  (branch "main")
  (introduction
    (make-channel-introduction
      "f9130e11e35d2c147c6764ef85542dc58dc09c4f"
      (openpgp-fingerprint
        "F164 709E 5FC7 B32B AEC7  9F37 1F2E 76AC E3F5 31C8"))))
```

If you are a [Guix Home](https://guix.gnu.org/en/manual/devel/en/html_node/Home-Configuration.html) user, check out the home-channels-service-type.

## Consider also visiting these channels
- [Akagi - Guix channel](https://git.sr.ht/~akagi/guixrc)
- [Hako - Rosenthal](https://codeberg.org/hako/Rosenthal)
- [Look - Saayix](https://codeberg.org/look/saayix)
